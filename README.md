# fundevogel.de

Hier findet Ihr den gesamten Code [unserer Webseite](https://fundevogel.de), einschließlich nützlicher Skripte (hauptsächlich in PHP und Python), mit denen wir Bilder optimieren & umbenennen, unsere Datenbestände aktualisieren, vergriffene Titel ausfindig machen und vieles mehr.

Viel Spaß beim Stöbern wünscht Euch allen

das Team vom Fundevogel

PS: Infos zum Datenschutz sowie unser Impressum und Bildquellen findet Ihr unter `content/datenschutz` bzw `content/impressum` oder [direkt hier](https://fundevogel.de/impressum).

:copyright: Fundevogel Kinder- und Jugendbuchhandlung
