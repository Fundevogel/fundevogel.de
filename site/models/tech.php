<?php

use Kirby\Http\Remote;

use Fundevogel\Thx\ThankYou;

class TechPage extends Page
{
    /**
     * Properties
     */

    /**
     * Request parameters
     *
     * @var array
     */
    public array $parameters = [
        'timeout' => 0,
        'headers' => ['User-Agent' => 'maschinenraum@fundevogel.de'],
    ];


    /**
     * Cache duration (in minutes)
     *
     * @var int
     */
    public int $cacheDuration = 14 * 24 * 60;  # two weeks


    /**
     * Methods
     */

    /**
     * Fetches data about 'Composer' packages in active use
     *
     * @return array
     */
    public function phpData(): array
    {
        # Access cache for 'tech' page
        $cache = kirby()->cache('tech');

        # Load data from cache
        $phpData  = $cache->get('phpData');

        # If nothing stored ..
        if (is_null($phpData)) {
            # .. prepare data retrieval
            $dataFile = kirby()->root('base') . '/composer.json';
            $lockFile = kirby()->root('base') . '/composer.lock';

            # Fetch 'Composer' packages
            $phpData = ThankYou::veryMuch($dataFile, $lockFile);

            # Sort them
            array_multisort(
                array_column($phpData, 'maintainer'), SORT_ASC,
                array_column($phpData, 'name'), SORT_ASC,
                $phpData
            );

            # Cache data
            $cache->set('phpData', $phpData, $this->cacheDuration);
        }

        return $phpData;
    }


    /**
     * Fetches data about 'NodeJS' packages in active use
     *
     * @return array
     */
    public function pkgData(): array
    {
        # Access cache for 'tech' page
        $cache = kirby()->cache('tech');

        # Load data from cache
        $pkgData  = $cache->get('pkgData');

        # If nothing stored ..
        if (is_null($pkgData)) {
            # .. prepare data retrieval
            $dataFile = kirby()->root('base') . '/package.json';
            $lockFile = kirby()->root('base') . '/package-lock.json';

            # Fetch 'Composer' packages
            $pkgData = ThankYou::veryMuch($dataFile, $lockFile);

            # Sort them
            array_multisort(
                array_column($pkgData, 'maintainer'), SORT_ASC,
                array_column($pkgData, 'name'), SORT_ASC,
                $pkgData
            );

            # Cache data
            $cache->set('pkgData', $pkgData, $this->cacheDuration);
        }

        return $pkgData;
    }


    /**
     * Fetches 'Pagespeed' score
     *
     * @return int 'Pagespeed' score
     */
    public function pageSpeed(): int
    {
        # Access cache for 'tech' page
        $cache = kirby()->cache('tech');

        # Load data from cache
        $score  = $cache->get('pageSpeed');

        # If nothing stored ..
        if (is_null($score)) {
            # .. fetch data from Google's PageSpeed API
            $pagespeedURL = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=https://fundevogel.de&category=performance';
            $pagespeedResponse = Remote::get($pagespeedURL, $this->parameters);

            # If everything goes as planned ..
            if ($pagespeedResponse->http_code() === 200) {
                # .. process response
                $pagespeedData = $pagespeedResponse->json(true);

                # .. determine 'PageSpeed' score
                $score = 100 * $pagespeedData['lighthouseResult']['categories']['performance']['score'];

                # Cache data
                $cache->set('pageSpeed', $score, $this->cacheDuration);
            }
        }

        return $score;
    }


    /**
     * Fetches 'Observatory' grade
     *
     * @return string 'Observatory' grade
     */
    public function observatory(): string
    {
        # Access cache for 'tech' page
        $cache = kirby()->cache('tech');

        # Load data from cache
        $grade  = $cache->get('observatory');

        # If nothing stored ..
        if (is_null($grade)) {
            # .. fetch data from Mozilla's Observatory API
            $observatoryURL = 'https://http-observatory.security.mozilla.org/api/v1/analyze?host=fundevogel.de&rescan=true';
            $post = Remote::post($observatoryURL, $this->parameters);

            while (true) {
                # Wait some time
                sleep(1);

                # Retrieve scan result
                $observatoryResponse = Remote::get($observatoryURL, $this->parameters);

                # If everything goes as planned ..
                if ($observatoryResponse->http_code() === 200) {
                    # .. process response
                    $observatoryData = $observatoryResponse->json(true);

                    # .. ensure test validity
                    if (isset($observatoryData['grade'])) {
                        # .. determine 'Observatory' grade
                        $grade = $observatoryData['grade'];

                        # Cache data
                        $cache->set('observatory', $grade, $this->cacheDuration);

                        # Abort execution
                        break;
                    }
                }
            }
        }

        return $grade;
    }


    /**
     * Fetches used programming languages
     *
     * @return array Language data
     */
    public function fetchLangData(): array
    {
        # Access cache for 'tech' page
        $cache = kirby()->cache('tech');

        # Load data from cache
        $langData  = $cache->get('langData');

        # If nothing stored ..
        if (is_null($langData)) {
            # .. fetch programming languages from Gitea's API as detected by `linguist`
            #
            # For unaccounted languages (eg, `yaml`),
            # see https://stackoverflow.com/a/26881503
            $langDataURL = 'https://codeberg.org/api/v1/repos/fundevogel/fundevogel.de/languages';
            $langDataResponse = Remote::get($langDataURL, $this->parameters);

            # If everything goes as planned ..
            if ($langDataResponse->http_code() === 200) {
                # .. process response
                $langData = $langDataResponse->json();

                # Cache data
                $cache->set('langData', $langData, $this->cacheDuration);
            }
        }

        return $langData;
    }
}
