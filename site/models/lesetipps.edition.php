<?php

use Kirby\Cms\Pages;
use Kirby\Data\Json;
use Kirby\Data\Yaml;
use Kirby\Toolkit\A;
use Kirby\Toolkit\Str;

class LesetippsEditionPage extends Page
{
    /**
     * Creates virtual child pages
     *
     * @return \Kirby\Cms\Pages
     */
    public function children(): Kirby\Cms\Pages
    {
        $children = [];

        if ($file = $this->data()->file('data.json')) {
            $count = 1;

            foreach (Json::read($file->root()) as $chapter => $books) {
                $data = [];

                # Add books to chapters ..
                foreach ($books as $book) {
                    $data[] = [
                        'title' => $book['header'][1] ?? $book['header'][0],
                        'author' => $book['header'][0],
                        'isbn' => $book['isbn'],
                        'body' => A::join($book['body'], "\n"),
                    ];
                }

                # .. and chapters as children
                $children[] = [
                    'slug'     => Str::slug($chapter),
                    'num'      => $count,
                    'template' => 'lesetipps.edition.chapter',
                    'model'    => 'lesetipps.edition.chapter',
                    'content'  => [
                        'title' => $chapter,
                        'books' => Yaml::encode($data),
                    ],
                ];

                $count++;
            }
        }

        return Pages::factory($children, $this);
    }


    /**
     * Stipulates virtual child pages
     *
     * @return \Kirby\Cms\Pages
     */
    public function subpages(): Kirby\Cms\Pages
    {
        return Pages::factory($this->inventory()['children'], $this);
    }


    /**
     * Fetches page representing current volume
     *
     * @return \Kirby\Cms\Page
     */
    public function volume(): Kirby\Cms\Page
    {
        return $this->siblings()->unlisted()->find($this->year()->value());
    }


    /**
     * Fetches edition data
     */
    public function data()
    {
        return $this->volume()->find(Str::slug($this->edition()->value())) ?? $this->volume();
    }


    /**
     * Fetches file object for current PDF edition
     *
     * @return Kirby\Cms\File
     */
    public function pdf(): Kirby\Cms\File
    {
        return $this->volume()->files()->filterBy('edition', $this->edition()->value())->first();
    }


    /**
     * Creates `img` tag from PDF edition
     *
     * @param string $classes Custom classes
     * @return string
     */
    public function getFront(string $classes = ''): string
    {
        return $this->pdf()->getFront($classes);
    }
}
