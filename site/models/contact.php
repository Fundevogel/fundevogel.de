<?php

class ContactPage extends Page
{
    /**
     * Provides opening hours for weekdays
     *
     * @return string
     */
    public function openWeekdays(): string
    {
        return sprintf('%s: %s - %s', t('Montag bis Freitag'), $this->weekStart()->time2locale(), $this->weekEnd()->time2locale(true));
    }


    /**
     * Provides opening hours for saturdays
     *
     * @return string
     */
    public function openSaturdays(): string
    {
        return sprintf('%s: %s - %s', t('Samstag'), $this->weekendStart()->time2locale(), $this->weekendEnd()->time2locale(true));
    }


    /**
     * Provides phone number
     *
     * @return string
     */
    public function phoneNumber(): string
    {
        return sprintf('%s: %s', t('Telefon'), site()->phone());
    }


    /**
     * Provides fax number
     *
     * @return string
     */
    public function faxNumber(): string
    {
        return sprintf('%s: %s', t('Fax'), site()->fax());
    }
}
