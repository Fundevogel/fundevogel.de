<?php

use Kirby\Data\Yaml;
use Kirby\Toolkit\Str;

use Nicebooks\Isbn\Isbn;
use Pcbis\Exceptions\NoRecordFoundException;

class LesetippsArticlePage extends Page
{
    /**
     * Creates page
     *
     * See https://getkirby.com/docs/reference/objects/cms/page/create
     *
     * @param array $props
     * @return \Kirby\Cms\Page
     */
    public static function create(array $props)
    {
        $isbn = $props['content']['title'];

        try {
            # Check if valid ISBN was provided
            $isbn = Isbn::of($isbn)->format();

            # Fetch information from API
            $data = loadBook($isbn);

            # Get shop link
            $data['shop'] = getShopLink($isbn);
        } catch (NoRecordFoundException $e) {
            # Fail for ISBNs not in database
            throw new Exception(sprintf('ISBN "%s" ist nicht in pcbis.de verfügbar!', $isbn));
        } catch (\Exception $e) {
            return parent::create($props);
        }

        # Determine template
        $template = 'book.default';

        if ($data['type'] == 'Hörbuch') {
            $template = 'book.audio';
        }

        if ($data['type'] == 'ePublikation') {
            $template = 'book.ebook';
        }

        $book = page('buecher')->createChild([
            'content' => $data,
            'slug' => Str::slug($data['title']) . ' ' . Str::slug($data['type']),
            'template' => $template,
        ]);

        return parent::create(array_merge($props, [
            'content' => [
                'title' => $data['title'],
                'book' => Yaml::encode($book->id()),
            ],
            'slug' => Str::slug($data['title']),
        ]));
    }


    /**
     * Creates file object from book cover
     *
     * @return \Kirby\Cms\File
     */
    public function getCover(): Kirby\Cms\File
    {
        return $this->book()->toPage()->getCover();
    }


    /**
     * Creates `img` tag from book cover
     *
     * @param string $classes Custom classes
     * @param bool $noLazy Whether image should be lazyloaded
     * @return string
     */
    public function getBookCover(string $classes = '', bool $noLazy = true): string
    {
        return $this->book()->toPage()->getBookCover($classes, $noLazy);
    }


    /**
     * Checks whether book has award
     *
     * @return bool
     */
    public function hasAward(): bool
    {
        if ($this->book()->isEmpty()) {
            return false;
        }

        return $this->book()->toPage()->hasAward()->bool();
    }


    /**
     * Fetches award information
     *
     * @return array
     */
    public function getAward(): array
    {
        return $this->book()->toPage()->getAward();
    }
}
