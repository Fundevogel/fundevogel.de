<?php

use Kirby\Cms\Pages;
use Kirby\Toolkit\Str;

class LesetippsArchivePage extends Page
{
    /**
     * Creates virtual child pages
     *
     * @return \Kirby\Cms\Pages
     */
    public function children(): Kirby\Cms\Pages
    {
        $children = [];

        foreach (parent::children()->filterBy('intendedTemplate', 'lesetipps.volume') as $volume) {
            foreach ($volume->files()->filterBy('template', 'pdf') as $edition) {
                $title = $edition->edition()->value() . ' ' . $edition->year()->value();

                $children[] = [
                    'slug'     => Str::slug($title),
                    'template' => 'lesetipps.edition',
                    'model'    => 'lesetipps.edition',
                    'content'  => [
                        'title'   => $title,
                        'edition' => $edition->edition()->value(),
                        'year'    => $edition->year()->value(),
                        'intro'   => $edition->intro()->value(),
                    ],
                ];
            }
        }

        return parent::children()->add(Pages::factory($children, $this));
    }


    /**
     * Stipulates virtual child pages
     *
     * @return \Kirby\Cms\Pages
     */
    public function subpages(): Kirby\Cms\Pages
    {
        return Pages::factory($this->inventory()['children'], $this);
    }
}
