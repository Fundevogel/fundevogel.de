<?php

use Kirby\Toolkit\Str;

class LesetippsEditionBookPage extends Page
{
    /**
     * Creates file object from book cover
     *
     * @return \Kirby\Cms\File
     */
    public function getCover(): Kirby\Cms\File
    {
        # Define filename
        $fileName = Str::slug($this->title()->value()) . '.jpg';

        # Grab image file
        $image = $this->parent()->parent()->data()->image($fileName);

        return $image ? $image : page('lesetipps')->fallback()->toFile();
    }


    /**
     * Applies necessary text processing
     *
     * @return string
     */
    public function text(): string
    {
        return Str::replace($this->body()->value(), ["\n", '-'], ['</p><p class="content">', '']);
    }
}
