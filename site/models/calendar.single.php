<?php

class CalendarSinglePage extends Page
{
    /**
     * Creates `img` tag from preview image
     *
     * @return string
     */
    public function getPreview(): string
    {
        $image = $this->previewImage()->isNotEmpty()
            ? $this->previewImage()->toFile()
            : $this->getCover()
        ;

        return $image->toTag('rounded-full transition-transform duration-350 transform group-hover:scale-110', 'calendar.single.preview', false, true);
    }
}
