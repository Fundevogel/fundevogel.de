<?php

class CalendarArchivePage extends Page
{
    /**
     * Uses 'calendar' cover as its own
     *
     * @return \Kirby\Cms\Field
     */
    public function cover(): Kirby\Cms\Field
    {
        return page('kalender')->cover();
    }
}
