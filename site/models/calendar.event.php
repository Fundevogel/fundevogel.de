<?php

use Kirby\Toolkit\A;

class CalendarEventPage extends Page
{
    /**
     * Creates printable string containing event date & time
     *
     * @return string
     */
    public function when(): string
    {
        $string = '';

        # Determine date(s)
        # (1) Start date
        $start = $this->date();
        $string .= $start->date2locale();

        # (2) End date (if specified)
        $end = $this->dateEnd();

        if ($end->toDate('Ymd') > $start->toDate('Ymd')) {
            $string .= ' - ' . $end->date2locale();
        }

        # Determine time(s)
        if ($start->toDate('Hi') != '0000') {
            # Start
            $string .= ', ' . $start->time2locale();

            # End (if specified)
            if ($end->toDate('Hi') != '0000') {
                $string .= ' - ' . $end->time2locale(true);
            }
        }

        return $string;
    }


    /**
     * Creates printable string containing event location, target audience & fees
     *
     * @return string
     */
    public function what(): string
    {
        $string = '';

        # Determine location
        if ($location = $this->location()) {
            $string .= $location->html() . '<br>';
        }

        # Determine recommended age
        if ($audience = $this->audience()) {
            $string .= $audience->html() . '<br>';
        }

        # Determine admission fee for ..
        if ($this->costsAdmission()->bool()) {
            $array = [];

            # (1) .. children
            if ($this->admissionChildren()->isNotEmpty()) {
                $array[] = sprintf('%s %s €', t('Kinder'), $this->admissionChildren());
            }

            if ($this->admissionAdults()->isNotEmpty()) {
                $array[] = sprintf('%s %s €', t('Erwachsene'), $this->admissionAdults());
            }

            $string .= sprintf('%s: %s', t('Eintritt'), A::join($array, ' &middot; '));
        }

        # .. if any
        else {
            $string .= t('Eintritt frei') . '!';
        }

        return $string;
    }


    /**
     * Creates printable string containing registration details
     *
     * @return string
     */
    public function registration(): string
    {
        # Build registration deadline
        $openUntil = '';

        if ($this->openUntil()->isNotEmpty()) {
            $openUntil .= ' bis zum ' . $this->openUntil()->date2locale();
        }

        # Build contact details
        $contact = '';

        if ($this->contact()->isNotEmpty()) {
            # (1) Prepare replacements
            $from = [
                'via Email',
                'telefonisch',
            ];

            $to = [
                sprintf('via <a href="mailto:%s">Email</a>', site()->mail()),
                sprintf('<a href="tel:%s">telefonisch</a>', site()->phone()->string2tel()),
            ];

            # (2) Replace last comma & prepend phrase
            $contact = ' am besten ' . Str::replace(substr_replace($this->contact(), ' oder', strrpos($this->contact(), ','), 1), $from, $to);
        }

        # Build required information
        $infos = '';

        if ($this->infos()->isNotEmpty()) {
            $infos = ' unter Angabe von ' . substr_replace($this->infos(), ' und', strrpos($this->infos(), ','), 1);
        }

        return sprintf('%s bitten wir Euch um <strong>vorherige Anmeldung %s</strong>, %s %s.', r($this->seats()->isEmpty(), 'Aufgrund der begrenzten Anzahl an Plätzen', 'Deshalb'), $openUntil, $contact, $infos);
    }


    /**
     * Creates printable string containing admission fees
     *
     * @return string
     */
    public function admission(): string
    {
        $admission = 'Der Eintritt ist frei!';

        if ($this->admissionChildren()->isNotEmpty() && $this->admissionAdults()->isNotEmpty()) {
            $admission = 'Der Eintritt für Kinder kostet ' . $this->admissionChildren() . ' Euro, Erwachsene zahlen ' . $this->admissionAdults() . ' Euro.';
        } else {
            if ($this->admissionChildren()->isNotEmpty()) {
                $admission = 'Der Eintritt für Kinder kostet ' . $this->admissionChildren() . ' Euro.';
            }

            if ($this->admissionAdults()->isNotEmpty()) {
                $admission = 'Der Eintritt für Erwachsene kostet ' . $this->admissionAdults() . ' Euro.';
            }
        }

        if ($this->admissionChildren()->toFloat() > 0 && $this->admissionChildren()->toFloat() == $this->admissionAdults()->toFloat()) {
            $admission = 'Der Eintritt beträgt für alle ' . $this->admissionChildren()->toFloat() . ' Euro.';
        }

        return $admission;
    }
}
