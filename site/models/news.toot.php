<?php

use Kirby\Toolkit\Str;

class NewsTootPage extends Page
{
    /**
     * Processes toot content
     *
     * @return string
     */
    public function getToot(): string
    {
        # Add class to paragraph & list elements
        # See https://forum.getkirby.com/t/add-classes-to-textarea-field-output/14060/5

        $from = [
            '/<p>/',
            '/<ul>/',
            '/<ol>/',
        ];

        $to = [
            '<p class="content">',
            '<ul class="list">',
            '<ol class="list">',
        ];

        return Str::replace(preg_replace($from, $to, $this->tootText()->value()), ' class="invisible"', '');
    }
}
