<?php snippet('layouts/images', compact('images', 'caption', 'preset'), slots: true) ?>

<?php slot('body') ?>
<hr>
<section class="container">
    <?= $page->about_us()->kt() ?>
</section>
<hr class="max-w-sm">
<section class="container">
    <div class="flex flex-col md:flex-row">
        <div class="mb-6 md:mb-0 flex-1">
            <?= $page->left()->kt() ?>
        </div>
        <div class="flex-1 md:ml-10">
            <?= $page->right()->kt() ?>
        </div>
    </div>
</section>
<?php endslot() ?>
