<?php snippet('layouts/default', slots: true) ?>

<?php slot('body') ?>
<hr id="aktuelle-veranstaltungen">
<section class="container">
    <h2 class="mb-12 text-center"><?= t('Alle vergangenen Veranstaltungen') ?></h2>
    <?php
        foreach($groupedEvents as $year => $events) : ?>
            <h3 class="mb-4 text-center"><?= $year ?></h2>
            <ul class="flex flex-col items-center">
                <?php foreach($events as $event) : ?>
                <li>
                    <time class="text-red-medium mr-2" datetime="<?= $event->date()->toDate('Y-m-d') ?>"><?= $event->date()->date2locale() ?></time>
                    <?= $event->title() ?>
                </li>
                <?php endforeach ?>
            </ul>
            <?php e($event !== $groupedEvents->last(), '<hr class="max-w-sm">') ?>
    <?php
        endforeach
    ?>
</section>
<?php endslot() ?>
