<?php snippet('layouts/default', slots: true) ?>

<?php slot('header') ?>
<?php snippet('layouts', ['layouts' => $page->layouts()->toLayouts()]) ?>
<?php endslot() ?>
