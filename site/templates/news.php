<?php snippet('layouts/default', slots: true) ?>

<?php slot('header') ?>
<header class="container">
    <?= $page->text()->kt() ?>
    <?php if ($page->hasCover()) : ?>
    <div class="mt-6">
        <figure class="group relative rounded-lg overflow-hidden">
            <?= $page->getCover()->toTag('rounded-lg transition-all', 'news.hero', false, true) ?>
            <figcaption class="big-caption sketch group-hover:-translate-y-full"><?= $page->getCover()->caption()->html() ?></figcaption>
        </figure>
    </div>
    <?php endif ?>
</header>
<?php endslot() ?>

<?php slot('body') ?>
<hr>
<section class="js-list">
    <h2 class="mb-12 text-center"><?= t('Neues aus dem Fundevogel') ?></h2>
    <?php foreach ($news as $article) : ?>
    <article id="<?= $article->slug() ?>" class="js-article animation-fade-in">
        <?php snippet(Kirby\Toolkit\Str::replace($article->intendedTemplate(), '.', '/'), compact('article')) ?>
        <?php e($article !== $articleLast, '<hr class="max-w-sm">', $nothingLeft) ?>
    </article>
    <?php endforeach ?>
</section>
<?php endslot() ?>

<?php slot('footer') ?>
<footer class="container">
    <nav class="js-hide mb-12 flex sketch text-5xl select-none">
        <?php if ($pagination->hasPrevPage()) : ?>
        <a class="h-20 flex-1 flex justify-around items-center text-white rounded-l-lg bg-red-light hover:bg-red-medium transition-all outline-none" href="<?= $pagination->prevPageURL() ?>" rel="prev" title="<?= t('Früheres--title') ?>">
            <?= useSVG(t('Früheres'), 'w-auto h-10 fill-current', 'arrow-left') ?>
            <span class="hidden md:inline"><?= t('Früheres') ?></span>
        </a>
        <?php else : ?>
        <span class="h-20 flex-1 rounded-l-lg bg-red-light opacity-75 cursor-not-allowed"></span>
        <?php
            endif;
            if ($pagination->hasNextPage()) :
        ?>
        <a class="js-target h-20 flex-1 flex justify-around items-center text-white rounded-r-lg bg-red-light hover:bg-red-medium transition-all outline-none" href="<?= $pagination->nextPageURL() ?>" rel="next" title="<?= t('Älteres--title') ?>">
            <span class="hidden md:inline"><?= t('Älteres') ?></span>
            <?= useSVG(t('Älteres'), 'w-auto h-10 fill-current', 'arrow-right') ?>
        </a>
        <?php else : ?>
        <span class="h-20 flex-1 rounded-r-lg bg-red-light opacity-75 cursor-not-allowed"></span>
        <?php endif ?>
    </nav>
    <button class="js-more w-full h-20 flex-1 flex justify-around items-center text-white rounded-lg bg-red-light hover:bg-red-medium transition-all outline-none nojs-hidden" type="button" title="<?= t('Frühere Neuigkeiten anzeigen') ?>">
        <span class="sketch text-5xl select-none"><?= t('Frühere Neuigkeiten') ?></span>
    </button>
</footer>
<?php endslot() ?>
