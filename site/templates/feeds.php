<?php snippet('layouts/default', slots: true) ?>

<?php slot('body') ?>
<hr>
<section class="container">
    <?= $page->details()->kt() ?>
</section>
<?php endslot() ?>
