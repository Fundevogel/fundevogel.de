<?php snippet('layouts/default', slots: true) ?>

<?php slot('header') ?>
<header class="container">
    <div class="flex flex-col lg:flex-row">
        <div class="flex-1">
            <div class="mb-8">
                <?= $page->text()->kt() ?>
            </div>
            <div class="mb-4 text-center">
                <h3><?= t('Kontaktinfos') ?></h3>
                <p>
                    Marienstraße 13, 79098 Freiburg<br>
                    <?= $page->phoneNumber() ?> &middot; <?= $page->faxNumber() ?><br>
                    Mail: <?= $site->mail() ?>
                </p>
            </div>
            <div class="text-center">
                <h3><?= t('Öffnungszeiten') ?></h3>
                <p>
                    <?= $page->openWeekdays() ?><br>
                    <?= $page->openSaturdays() ?>
                </p>
            </div>
        </div>
        <div class="mt-12 flex-none flex justify-center items-center">
            <?php
                if ($page->hasCover()) :
                $images = new Files([$page->cover()->toFile()]);
                $caption = $page->getCover()->caption()->html();
            ?>
            <div class="lg:ml-12">
                <?php snippet('base/lightbox', compact('images', 'caption')) ?>
            </div>
            <?php endif ?>
        </div>
    </div>
</header>
<?php endslot() ?>

<?php slot('body') ?>
<aside class="wave">
    <?= useSeparator('orange-light', 'top-reversed') ?>
    <div class="inner">
        <div class="container xl:px-8">
            <div class="text-center">
                <?= useSVG(t('Kontakt-Überschrift'), 'title-icon', 'map-filled') ?>
            </div>
            <h2 class="title text-orange-medium"><?= t('Kontakt-Überschrift') ?></h2>
            <div class="flex flex-col lg:flex-row">
                <div class="lg:w-2/5 lg:mr-12 mb-6">
                    <h3 class="mb-2 text-center text-orange-medium"><?= t('Mit dem Fahrrad') ?></h3>
                    <?= $page->bike()->kt() ?>
                </div>
                <div class="flex-1 flex flex-col">
                    <div class="mb-4">
                        <h3 class="mb-2 text-center text-orange-medium"><?= t('Über den Asphalt') ?></h3>
                        <?= $page->car()->kt() ?>
                    </div>
                    <div class="mb-6">
                        <h3 class="mb-2 text-center text-orange-medium"><?= t('Auf der Schiene') ?></h3>
                        <?= $page->tram()->kt() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= useSeparator('orange-light', 'bottom-reversed') ?>
</aside>
<section class="container">
    <div class="mb-8">
        <?= $page->formText()->kt() ?>
    </div>
    <?php if (!$form->success() && count($form->errors()) > 0) : ?>
    <div class="mb-8">
        <?= $page->formError()->kt() ?>

        <ul class="list">
            <?php foreach ($form->errors() as $error) : ?>
            <li class="font-medium"><?= implode('<br>', $error) ?></li>
            <?php endforeach ?>
        </ul>
    </div>
    <?php endif ?>

    <form method="POST">
        <div class="flex flex-col md:flex-row">
            <div class="flex-1 mb-4 md:mr-2">
                <label for="subject" class=""><?= t('Betreff') ?></label>
                <input
                    class="form-input placeholder-orange-medium placeholder-opacity-100 focus:outline-none active:outline-none<?php e($form->error('subject'), ' error') ?>"
                    id="subject"
                    name="subject"
                    type="text"
                    value="<?= $form->old('subject') ?>"
                >
            </div>
            <div class="flex-1 mb-4 md:ml-2">
                <div class="flex justify-between">
                    <label for="email">E-Mail <span class="font-bold text-red-medium">*</span></label>
                    <span class="font-bold text-xs text-red-medium">*<?= t('erforderlich') ?></span>
                </div>
                <input
                    class="form-input placeholder-orange-medium placeholder-opacity-100 focus:outline-none active:outline-none<?php e($form->error('email'), ' error') ?>"
                    id="email"
                    name="email"
                    type="email"
                    value="<?= $form->old('email') ?>"
                >
            </div>
        </div>
        <div class="mb-4">
            <div class="flex justify-between">
                <label for="message"><?= t('Nachricht') ?> <span class="font-bold text-red-medium">*</span></label>
                <span class="font-bold text-xs text-red-medium">*<?= t('erforderlich') ?></span>
            </div>
            <textarea
                class="form-input placeholder-orange-medium placeholder-opacity-100 focus:outline-none active:outline-none<?php e($form->error('message'), ' error') ?>"
                id="message"
                name="message"
                rows="8"
            ><?= $form->old('message') ?></textarea>
        </div>
        <?= csrf_field() ?>
        <?= honeypot_field() ?>
        <input
            class="w-full h-16 px-4 flex justify-center items-center sketch text-2xl text-white text-shadow bg-red-light hover:bg-red-medium transition-all cursor-pointer rounded-lg"
            type="submit"
            value="<?= t('Absenden') ?>"
        >
    </form>
</section>
<?php endslot() ?>
