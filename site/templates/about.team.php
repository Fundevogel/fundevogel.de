<?php snippet('layouts/default', slots: true) ?>

<?php slot('body') ?>
<hr>
<section class="container">
    <h2 class="mb-12 text-center"><?= t('Wir stellen uns vor') ?></h2>
    <div class="js-masonry">
        <?php foreach ($team as $member) : ?>
        <div class="card">
            <div class="flex flex-col items-center">
                <figure class="mb-6 relative">
                    <?php if ($member->back()->isNotEmpty() && $member->front()->isNotEmpty()) : ?>
                    <div class="group">
                        <?= $member->back()->toFile()->toTag('block group-hover:hidden rounded-md', 'about.team', false, true) ?>
                        <?= $member->front()->toFile()->toTag('hidden group-hover:block rounded-md', 'about.team', false, true) ?>
                    </div>
                    <?php elseif ($member->back()->isNotEmpty()) : ?>
                    <?= $member->back()->toFile()->toTag('rounded-md cursor-not-allowed', 'about.team') ?>
                    <?php else : ?>
                    <?= $page->fallback()->toFile()->toTag('rounded-md', 'about.team') ?>
                    <?php endif ?>
                    <?php if ($member->isActive()->bool()) : ?>
                    <span class="js-tippy group flex absolute h-6 w-6 top-0 right-0 -mt-2 -mr-2" title="aktiv" data-tippy-theme="fundevogel red">
                        <span class="group-hover:animate-ping absolute inline-flex h-full w-full rounded-full bg-red-medium opacity-50"></span>
                        <span class="relative inline-flex rounded-full h-6 w-6 bg-red-medium"></span>
                    </span>
                    <?php endif ?>
                </figure>
                <!-- <?php if ($member->employment()->isNotEmpty()) : ?>
                <span class="font-medium text-xs"><?= $member->employment() ?></span>
                <?php endif ?> -->
                <h3 class=""><?= $member->name() ?></h3>
                <div class="text-sm text-center">
                    <?= $member->desc()->kt() ?>
                </div>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</section>
<?php endslot() ?>
