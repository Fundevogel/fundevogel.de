<?php snippet('layouts/default', slots: true) ?>

<?php slot('body') ?>
<hr>
<?php snippet('layouts', compact('layouts')) ?>
<?php endslot() ?>

<?php slot('footer') ?>
<footer class="mt-16">
<div class="container">
    <nav class="flex sketch text-5xl select-none">
        <?php
            foreach ($siblings as $sibling) :
            $isLeft = $sibling === $siblings->first();
        ?>
        <a
            class="h-20 flex-1 flex justify-around items-center text-white text-shadow <?php e($isLeft, 'rounded-l-lg ', 'rounded-r-lg ') ?>bg-red-light hover:bg-red-medium transition-all outline-none"
            href="<?= $sibling->url() ?>"
            rel="<?php e($isLeft, 'prev', 'next') ?>"
            title="<?= $sibling->title()->html() ?>"
        >
            <?php if ($isLeft) : ?>
                <?= useSVG($sibling->title()->html(), 'w-auto h-10 fill-current', 'arrow-left') ?>
            <?php endif ?>
            <span class="hidden md:inline"><?= $sibling->handle()->html() ?></span>
            <?php if (!$isLeft) : ?>
                <?= useSVG($sibling->title()->html(), 'w-auto h-10 fill-current', 'arrow-right') ?>
            <?php endif ?>
        </a>
        <?php endforeach ?>
    </nav>
</div>
</footer>
<?php endslot() ?>
