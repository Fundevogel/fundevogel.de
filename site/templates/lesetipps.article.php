<?php snippet('layouts/default', slots: true) ?>

<?php slot('header') ?>
<header class="container">
    <time datetime="<?= $page->date()->toDate('Y-m-d') ?>"><?= $page->date()->date2locale() ?></time>
    <h2><?= $page->title()->html() ?></h2>
    <?php if ($page->hasAward()) snippet('lesetipps/award') ?>
    <?= $page->text()->kt() ?>
</header>
<?php endslot() ?>

<?php slot('body') ?>
<?php if ($page->isAdvanced()->bool()) : ?>
<?php snippet('layouts', compact('layouts')) ?>
<?php else : ?>
<?php snippet('base/books', $data) ?>
<?php if ($page->conclusion()->isNotEmpty()) : ?>
<section class="container">
    <?= $page->conclusion()->kt() ?>
</section>
<?php endif ?>
<?php endif ?>
<?php if ($page->hasAward()) : ?>
<aside class="container">
    <div class="mt-12 px-8 py-6 card is-dashed">
        <?php snippet('lesetipps/' . $page->getAward()['identifier']) ?>
    </div>
</aside>
<?php endif ?>
<?php if ($page->hasTranslatedSiblings()) : ?>
<footer class="mt-16">
    <?php snippet('lesetipps/prevnext') ?>
</footer>
<?php endif ?>
<?php endslot() ?>
