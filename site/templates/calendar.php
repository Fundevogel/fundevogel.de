<?php snippet('layouts/default', slots: true) ?>

<?php slot('header') ?>
<header class="container">
    <div class="flex flex-col lg:flex-row">
        <div class="flex-1">
            <?= $page->text()->kt() ?>
        </div>
        <?php if ($page->hasCover()) : ?>
        <div class="mt-12 flex-none flex justify-center">
            <div>
                <a class="lg:ml-12 group table relative" href="<?= $page->webCal() ?>" data-barba-prevent="self">
                    <figure class="inline-block rounded-lg">
                        <?= $page->getCover()->toTag('rounded-t-lg', 'cover', false, true) ?>
                        <figcaption class="small-caption"><?= $page->getCover()->caption()->html() ?></figcaption>
                    </figure>
                    <?php snippet('partials/overlay', [
                        'caption' => t('Aktuelle Veranstaltungen'),
                        'icon' => 'calendar-filled',
                        'details' => t('als iCal-Datei abonnieren')
                    ]) ?>
                </a>
            </div>
        </div>
        <?php endif ?>
    </div>
</header>
<?php endslot() ?>

<?php slot('body') ?>
<hr>
<section class="container">
    <h2 id="<?= Kirby\Toolkit\Str::lower(t('Veranstaltungen')) ?>" class="mb-12 text-center"><?= t('Alle Veranstaltungen im Überblick') ?></h2>
    <?php if ($openEvents->isNotEmpty()) : ?>
    <?php foreach($openEvents as $timeRange => $events) : ?>
    <h2 class="mb-8 text-center"><?= $timeRange ?></h2>
    <?php foreach($events as $event) : ?>
    <article class="">
        <div class="flex flex-col lg:flex-row">
            <div class="flex-1">
                <h3><?= $event->title()->html() ?></h3>
                <?= $event->text()->kt() ?>
            </div>
            <aside class="lg:ml-10 pt-4 lg:pt-10 w-full lg:max-w-xs">
                <?php snippet('calendar/quickview', compact('event')) ?>
            </aside>
        </div>
        <?php if ($event->details()->isNotEmpty()) : ?>
        <div class="mt-12">
            <?= $event->details()->kt() ?>
        </div>
        <?php endif ?>
        <?php if ($event->registrationRequired()->bool()) : ?>
        <aside class="mt-12 px-8 py-6 card is-dashed">
            <h4>Hinweise zur Veranstaltung</h4>
            <ul class="list">
                <?php if ($event->seats()->isNotEmpty()) : ?>
                <li>Die Veranstaltung ist auf <?= $event->seats() ?> Plätze begrenzt!</li>
                <?php endif ?>
                <li><?= $event->registration() ?></li>
                <?php if ($event->costsAdmission()->bool()) : ?>
                <li><?= $event->admission() ?></li>
                <?php endif ?>
                <?php if ($event->note()->isNotEmpty()) : ?>
                <li><?= $event->note() ?></li>
                <?php endif ?>
                <?php if ($event->link()->isNotEmpty()) : ?>
                <li>Weitere Informationen zur Veranstaltung findet Ihr <a href="<?= $event->link() ?>" target="_blank">hier</a>.</li>
                <?php endif ?>
            </ul>

        </aside>
        <?php endif ?>

    </article>
    <?php e($event !== $events->last(), '<hr class="max-w-xs">') ?>
    <?php endforeach ?>
    <?php e($event === $events->last() && $timeRange === t('In der Ferne'), '<hr class="max-w-sm">') ?>
    <?php endforeach ?>
    <?php else : ?>
    <p class="italic text-center"><?= t('Keine Veranstaltungen') ?></p>
    <?php endif ?>
</section>
<?php if ($kirby->collection('events/annual')->isNotEmpty()) : ?>
<aside class="wave">
    <?= useSeparator('orange-light', 'top-reversed') ?>
    <div class="pt-12 pb-6 lg:pb-4 bg-orange-light">
        <div class="container lg:px-8 xl:px-12">
            <div class="text-center">
                <?= useSVG(t('Jährliche Höhepunkte'), 'title-icon', 'calendar-filled') ?>
            </div>
            <h2 class="title text-orange-medium"><?= t('Jährliche Höhepunkte') ?></h2>
            <div class="flex flex-col lg:flex-row">
                <?php foreach ($kirby->collection('events/annual') as $event) : ?>
                <div class="<?php e($event->num() == 2, 'lg:mx-12 xl:mx-16 ') ?>mb-8">
                    <div class="mb-8 flex justify-center">
                        <a class="rounded-full group overflow-hidden" href="<?= $event->url() ?>">
                            <?= $event->getPreview() ?>
                        </a>
                    </div>
                    <div class="text-center">
                        <h3>
                            <a class="text-orange-medium hover:text-orange-dark" href="<?= $event->url() ?>">
                                <?= $event->title()->html() ?>
                            </a>
                        </h3>
                        <?= $event->previewDescription()->kt() ?>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    <?= useSeparator('orange-light', 'bottom') ?>
</aside>
<?php endif ?>
<?php if ($kirby->collection('events/closed')->isNotEmpty()) : ?>
<section class="container">
    <h2 class="mb-12 text-center"><?= t('Geschlossene Veranstaltungen') ?></h2>
    <div class="js-masonry">
        <?php foreach ($kirby->collection('events/closed') as $event) : ?>
        <div class="card">
            <h4><?= $event->title() ?></h4>
            <?php snippet('calendar/quickview', compact('event')) ?>
        </div>
        <?php endforeach ?>
    </div>
</section>
<?php endif ?>
<?php endslot() ?>
