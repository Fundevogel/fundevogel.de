<?php layout('listing', compact('heading', 'cards')) ?>

<?php slot('header') ?>
<header class="container">
    <div class="flex flex-col lg:flex-row">
        <div class="flex-1">
            <?= $page->text()->kt() ?>
        </div>
        <div class="mt-12 flex-none text-center">
            <p class="max-w-md mb-10 font-bold font-small-caps text-center text-red-medium inline-block lg:hidden">
                <?= $page->motto()->html() ?>
            </p>
            <?php snippet('cover') ?>
        </div>
    </div>
    <div class="mt-12 text-center">
        <p class="max-w-md font-bold font-small-caps text-red-medium hidden lg:inline-block">
            <?= $page->motto()->html() ?>
        </p>
    </div>
</header>
<?php endslot() ?>
