<?php snippet('layouts/images', compact('images', 'caption'), slots: true) ?>
    <?php slot('body') ?>
    <hr>
    <section class="container">
        <?= $page->details()->kt() ?>
    </section>
    <hr>
    <section class="container">
        <h2 class="mb-0 text-center"><?= t('Sortiment-Überschrift') ?></h2>
        <?php
            $count = 0;
            foreach ($kirby->collection('assortment') as $category) :
        ?>
        <div class="mt-16 flex flex-col lg:flex-row">
            <?php if ($category->hasCover()) : ?>
            <div class="flex-none text-center <?php e($count % 2 == 0, 'lg:order-last', 'lg:order-first') ?>">
                <a class="" href="<?= $category->url()?>">
                    <figure class="group inline-block <?php e($count % 2 == 0, 'lg:ml-12', 'lg:mr-12') ?> rounded-lg overflow-hidden">
                        <?= $category->getCover()->toTag('rounded-lg transition-transform duration-350 transform group-hover:scale-110', 'assortment.navigation', false) ?>
                    </figure>
                </a>
            </div>
            <?php endif ?>
            <div class="flex-1 mt-8 lg:mt-0 flex flex-col justify-center">
                <h3><a class="link" href="<?= $category->url() ?>"><?= $category->title()->html() ?></a></h3>
                <?= $category->short()->kt() ?>
                <?= $category->moreLink('link font-bold font-small-caps text-sm outline-none') ?>
            </div>
        </div>
        <?php e($category !== $kirby->collection('assortment')->last(), '<hr class="max-w-sm">') ?>
        <?php
            $count++;
            endforeach;
        ?>
    </section>
    <?php endslot() ?>

<?php endsnippet() ?>
