<?php

use Kirby\Toolkit\A;

use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Fundevogel\Pcbis\Pcbis;


/**
 * Connects to KNV's Webservice API
 *
 * @return \Fundevogel\Pcbis\Pcbis
 */
function pcbis(): \Fundevogel\Pcbis\Pcbis
{
    # Initializing Webservice object
    return new Pcbis([
        'VKN' => env('knv_vkn'),
        'Benutzer' =>env('knv_username'),
        'Passwort' => env('knv_password'),
    ]);
}


/**
 * Fetches book data
 *
 * @param string $isbn ISBN
 * @return array
 * @throws \Exception No record found
 */
function loadBook(string $isbn): array
{
    # Fetch book data
    $book = pcbis()->load($isbn);

    if (is_null($book)) {
        throw new \Exception('No record found!');
    }

    # Basic dataset
    $data = [
        'type'         => $book->type(),
        'isbn'         => $book->isbn(),
        'title'        => $book->title()->value(),
        'subtitle'     => $book->subtitle()->value(),
        'description'  => $book->description()->value(),
        'price'        => $book->retailPrice()->value(),
        'releaseYear'  => $book->releaseYear()->value(),
        'age'          => $book->age()->value(),
        'categories'   => $book->categories()->value(),
        'topics'       => $book->topics()->value(),
        'publisher'    => $book->publisher()->value(),
        'isSeries'     => $book->isSeries(),
        'series'       => $book->series(),
        // 'volume'       => $book->volume(),
        'author'       => $book->getRole('author')->value(),
        'illustrator'  => $book->getRole('illustrator')->value(),
        'drawer'       => $book->getRole('drawer')->value(),
        'photographer' => $book->getRole('photographer')->value(),
        'translator'   => $book->getRole('translator')->value(),
        'editor'       => $book->getRole('editor')->value(),
        'participant'  => $book->getRole('participant')->value(),
    ];

    # Extended dataset: book
    if ($book->isBook()) {
        $data = A::update($data, [
            'binding'   => $book->binding()->value(),
            'pageCount' => $book->pageCount()->value(),
            'antolin'   => $book->antolin()->value(),
        ]);
    }

    # Extended dataset: audiobook
    if ($book->isAudiobook()) {
        $data = A::update($data, [
            'duration' => $book->duration()->value(),
            'narrator' => $book->getRole('narrator')->value(),
            'composer' => $book->getRole('composer')->value(),
            'director' => $book->getRole('director')->value(),
            'producer' => $book->getRole('producer')->value(),
        ]);
    }

    # Extended dataset: ebook
    if ($book->isEbook()) {
        $data = A::update($data, [
            'devices'    => $book->devices()->value(),
            'print'      => $book->printEdition()->value(),
            'fileSize'   => $book->fileSize()->value(),
            'fileFormat' => $book->fileFormat()->value(),
            'drm'        => $book->drm()->value(),
        ]);
    }

    return $data;
}


/**
 * Fetches shop link
 *
 * @param string $isbn ISBN
 * @param string $provider Provider
 * @param string $storeID Store ID
 * @param string $catalogID Catalog ID
 * @return string
 */
function getShopLink(string $isbn, string $provider = 'fundevogel', string $storeID = '63715', string $catalogID = '10002'): string
{
    # Define query parameters
    $params = [
        'storeId=' . $storeID,
        'catalogId=' . $catalogID,
        'langId=-3',
        'pageSize=10',
        'beginIndex=0',
        'sType=SimpleSearch',
        'resultCatEntryType=2',
        'showResultsPage=true',
        'pageView=',
        'pageType=PK',
        'searchTerm=' . $isbn,
        'searchBtn=',
        'mediaTypes=All%20Media',
    ];

    # Build URL
    $url = sprintf('https://%s.buchkatalog.de/servlet/SearchDisplay?%s', $provider, A::join($params, '&'));

    # Send GET request
    $response = (new Client())->get($url, ['allow_redirects' => false, 'headers' => ['User-Agent' => 'fundevogel.de']]);

    # If 'Location' header is present ..
    if ($response->hasHeader('Location')) {
        # .. extract & process target URL
        return rtrim($response->getHeaders()['Location'][0], '01234567890/%');
    }

    return '';
}
