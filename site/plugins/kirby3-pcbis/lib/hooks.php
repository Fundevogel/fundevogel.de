<?php

return [
    /**
     * See https://getkirby.com/docs/reference/plugins/hooks/page-create-after
     *
     * @param Kirby\Cms\Page $page
     * @return void
     */
    'page.create:after' => function (Kirby\Cms\Page $page)
    {
        $templates = [
            'book.audio',
            'book.default',
            'book.ebook',
        ];

        if (in_array((string) $page->intendedTemplate(), $templates)) {
            $page->changeStatus('listed');

            janitor('downloadCover', $page, $page->id());
        }
    },
];
