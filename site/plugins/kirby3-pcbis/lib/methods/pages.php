<?php

return [
    /**
     * Fetches translations for current page
     *
     * @return Kirby\Cms\Pages
     */
    'onlyTranslated' => function (string $language = null): Kirby\Cms\Pages
    {
        if ($language === null) {
            $language = kirby()->language()->code();
        }

        return $this->filter(function ($page) use ($language) {
            if ($page->isTranslated($language)) {
                return $page;
            }
        });
    },


    /**
     * Filters books in pages field 'book'
     *
     * @param string $field Field name
     * @param string $value Field value
     * @return Kirby\Cms\Pages
     */
    'filterBooks' => function (string $field, string $value)
    {
        return $this->filter(function ($page) use ($field, $value) {
            $books = $page->book()->toPages()->filterBy($field, $value, ',');

            if ($books->isNotEmpty()) {
                return $page;
            }
        });
    },
];
