<?php

return [
    /**
     * Fetches bibliographic data for current page's ISBN
     *
     * @param bool $forceRefresh Whether cached data should be overwritten
     * @return \Pcbis\Products\Books\Book|false
     */
    'toBook' => function (bool $forceRefresh = false)
    {
        if ($this->isbn()->isEmpty()) {
            return false;
        }

        return pcbis()->load($this->isbn()->value());
    },


    /**
     * Updates page's bibliographic data
     *
     * @param bool $forceRefresh Whether cached data should be overwritten
     * @param array $refresh Page fields to be updated
     * @return bool Whether update was successful
     */
    'updateBook' => function (bool $forceRefresh = false, array $refresh = []): bool
    {
        # Load data
        $data = loadBook($this->isbn()->value());

        # Build update array from fetched data
        $updateArray = [];

        foreach ($data as $key => $value) {
            # Don't update ..
            # (1) .. fields that are filled and not explicitly eligible for refreshing
            if ($this->$key()->isNotEmpty() && !in_array($key, $refresh)) {
                continue;
            }

            # (2) .. `subtitle` if the `author` field is filled
            $hasAuthor = $this->author()->isNotEmpty();

            if ($key === 'subtitle' && $hasAuthor) {
                continue;
            }

            $updateArray[$key] = $value;
        }

        # Only request shop URL if its field is currently empty
        if ($this->shop()->isEmpty() && !in_array('shop', $refresh)) {
            $updateArray['shop'] = getShopLink($this->isbn()->value());
        }

        try {
            $this->update($updateArray);

            return true;

        } catch (\Exception $e) {}

        return false;
    },


    /**
     * Fetches OLA object for current page's ISBN
     *
     * @param int $quantity Quantity to be checked
     * @return \Pcbis\Api\Ola
     */
    'toOla' => function (int $quantity = 1): Pcbis\Api\Ola
    {
        return pcbis()->ola($this->isbn()->value(), $quantity);
    },


    /**
     * Updates OLA status for current page's ISBN
     *
     * @param int $quantity Quantity to be checked
     * @return bool Whether update was successful
     */
    'updateOla' => function (int $quantity = 1): bool
    {
        # Request OLA
        $ola = pcbis()->ola($this->isbn()->value(), $quantity);

        # Build update array from OLA request
        $updateArray = ['isAvailable' => $ola->isAvailable()];

        if ($ola->hasOlaCode()) {
            $updateArray['olaCode'] = $ola->olaCode();
        }

        if ($ola->hasOlaMessage()) {
            $updateArray['olaMessage'] = $ola->olaMessage();
        }

        try {
            $this->update($updateArray);

            return true;

        } catch (\Exception $e) {}

        return false;
    },


    /**
     * Upgrades page's book
     *
     * @return bool Whether upgrade was successful
     */
    'upgradeBook' => function (): bool
    {
        $book = $this->toBook();

        if (!$book->hasUpgrade()) {
            throw new Exception('Upgrade nicht erforderlich!');
        }

        $book = $book->upgrade();

        try {
            $this->update([
                'isbn'        => $book->isbn(),
                'releaseYear' => $book->releaseYear(),
                'olaCode'     => $book->statusCode(),
                'olaMessage'  => $book->statusMessage(),
                'shop'        => getShopLink($book->isbn()),
            ]);

            return true;

        } catch (\Exception $e) {}

        return false;
    },
];
