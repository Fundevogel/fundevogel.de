<?php

use Kirby\Toolkit\Str;

use Nicebooks\Isbn\Isbn;
use Pcbis\Exceptions\NoRecordFoundException;


class BookPage extends Page
{
    /**
     * Creates page
     *
     * See https://getkirby.com/docs/reference/objects/cms/page/create
     *
     * @param array $props
     * @return \Kirby\Cms\Page
     */
    public static function create(array $props): Kirby\Cms\Page
    {
        $isbn = $props['content']['title'];

        try {
            # Check if valid ISBN was provided
            $isbn = Isbn::of($isbn)->format();

            # Fetch information from API
            $data = loadBook($isbn);

            # Add shop link
            $data['shop'] = getShopLink($isbn);

        } catch(\Exception $e) {
            if ($e->getMessage() == 'No record found!') {
                # Fail for ISBNs not in database
                throw new \Exception(sprintf('ISBN "%s" ist nicht in pcbis.de verfügbar!', $isbn));
            }

            return parent::create($props);
        }

        # Determine template
        $template = 'book.default';

        if (isset($data['type'])) {
            if ($data['type'] == 'Hörbuch') {
                $template = 'book.audio';
            }

            if ($data['type'] == 'ePublikation') {
                $template = 'book.ebook';
            }
        }

        return parent::create(array_merge($props, [
            'slug' => Str::slug($data['title']) . ' ' . Str::slug($data['type']),
            'template' => $template,
            'content' => $data,
        ]));
    }


    /**
     * Checks whether book is hardover/paperback
     *
     * @return bool
     */
    public function isBook(): bool
    {
        return $this->intendedTemplate() == 'book.default';
    }


    /**
     * Checks whether book is audiobook
     *
     * @return bool
     */
    public function isAudiobook(): bool
    {
        return $this->intendedTemplate() == 'book.audio';
    }


    /**
     * Checks whether book is ebook
     *
     * @return bool
     */
    public function isEbook(): bool
    {
        return $this->intendedTemplate() == 'book.ebook';
    }


    /**
     * Fetches award information
     *
     * @return array
     */
    public function getAward(): array
    {
        $award = '';

        if (Str::contains(Str::lower($this->award()), 'lesepeter')) {
            $award = 'lesepeter';
        }

        if (Str::contains(Str::lower($this->award()), 'wolgast')) {
            $award = 'wolgast';
        }

        if ($award === '') {
            return [];
        }

        $array = site()->awards()
                       ->toStructure()
                       ->filterBy('identifier', $award)
                       ->first()
                       ->toArray();

        $array['awardlink'] = $this->leselink()->toUrl();
        $array['awardtitle'] = $this->award()->value() . ' ' . $this->awardEdition()->value();

        return $array;
    }
}
