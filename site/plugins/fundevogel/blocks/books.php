<?php

if ($block->books()->isEmpty()) {
    return false;

}

$data = [
    'data' => $block->books()->toStructure(),
    'heading' => $block->heading(),
    'icon' => $block->icon(),
    'useDetails' => $block->useDetails()->bool(),
    'useTaxonomy' => $block->useTaxonomy()->bool(),
];

snippet('base/books', $data);
