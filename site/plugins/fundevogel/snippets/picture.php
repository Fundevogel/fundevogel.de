<picture>
    <?php
        foreach (option('thumbs.formats') as $format) :
        foreach ($sizes as $max) :

        $options = A::update(option('thumbs.presets')[$preset . '.' . $max], ['format' => $format]);
        $thumb = $src->thumb($options);
    ?>
    <source
        media="(min-width: <?=$max?>px)"
        type="<?= $thumb->mime() ?>"
        <?php if ($noLazy === true) : ?>
        srcset="<?= $thumb->url() ?>"
        <?php else : ?>
        data-srcset="<?= $thumb->url() ?>"
        <?php endif ?>
    >
    <?php
        endforeach;
        endforeach;

        foreach ($sizes as $max) :
        $image = $src->thumb($preset . '.' . $max);
    ?>
    <source
        media="(min-width: <?= $max?>px)"
        type="<?= $image->mime() ?>"
        <?php if ($noLazy === true) : ?>
        srcset="<?= $image->url() ?>"
        <?php else : ?>
        data-srcset="<?= $image->url() ?>"
        <?php endif ?>
    >
    <?php endforeach?>

    <?= $tag ?>
</picture>
