<div class="card is-dashed text-center">
    <h4 class="text-base"><?= t('Termin im Überblick') ?></h4>
    <p class="content text-sm">
        <?= $event->when() ?><br>
        <?= $event->what() ?><br>
        <a
            class="mt-4 flex justify-center items-center"
            href="<?= $event->url() . '.ics' ?>"
            data-barba-prevent="self"
        >
            <?= useSVG($event->title(), 'w-6 h-6 fill-current', 'calendar-filled') ?>
            <span class="ml-2">
                <?= t('iCal-Datei herunterladen') ?>
            </span>
        </a>
        <?php if ($event->coordinates()->isNotEmpty()) : ?>
        <a
            class="mt-2 flex justify-center items-center"
            href="https://www.openstreetmap.org/node/<?= $event->coordinates()->toLocation()->osm() ?>"
        >
            <?= useSVG($event->location(), 'w-6 h-6 fill-current', 'map-filled') ?>
            <span class="ml-2">OpenStreetMap</span>
        </a>
        <?php endif ?>
    </p>
</div>
