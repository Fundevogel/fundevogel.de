<?php

use Kirby\Data\Json;
use Kirby\Data\Yaml;
use Kirby\Toolkit\Str;


/**
 * SVG manipulation
 */

/**
 * Generates SVG icon string
 *
 * @param string $title Title attribute
 * @param string $classes Classes
 * @param string $file SVG filename
 * @param string $customAttribute Custom attributes
 * @return string
 */
function useSVG(?string $title = null, string $classes = '', string $file = '', string $customAttribute = ''): string
{
    if (empty($title)) {
        $title = 'SVG';
    }

    if (empty($file)) {
        $file = Str::lower(Str::replace($title, '-', ''));
    }

    $svgPath = 'assets/icons/' . $file . '.svg';
    $svg = (new Asset($svgPath))->read();

    return Str::replace($svg, '<svg', '<svg class="' . $classes . '" title="' . $title . '" role="img"' . r($customAttribute !== '', ' ' . $customAttribute), 1);
}


/**
 * Generates SVG separator string
 *
 * @param string $color Color name
 * @param string $position position
 * @return string
 */
function useSeparator(string $color = 'orange-light', string $position = 'top'): string
{
    $svg = (new Asset(sprintf('assets/icons/%s.svg', $position)))->read();

    $margin = Str::contains($position, 'top') === true ? '-mb-px' : '-mt-px';

    return '<div class="w-full">' . Str::replace($svg, '<svg', '<svg class="w-full h-auto ' . $margin . ' fill-current text-' . $color .'" role="img"', 1) . '</div>';
}


/**
 * Language support
 */

/**
 * Fetches all available translations for given language
 *
 * @param string $code Language code
 * @return array
 */
function getTranslations(?string $code = null): array
{
    # If language code not provided ..
    if (empty($code)) {
        # .. use current language
        $code = kirby()->languageCode();
    }

    # Determine path to data file
    $file = sprintf('%s/translations/%s.yml', kirby()->root('languages'), $code);

    return Yaml::read($file);
}


/**
 * Fetches settings (eg date & time formats) for given language
 *
 * @param string $code Language code
 * @return array
 */
function getLanguageSettings(?string $code = null): array
{
    # Determine path to data file
    $file = sprintf('%s/settings/formats.json', kirby()->root('languages'));

    # Fetch language data
    # TODO: Currently this cannot through the `$language` object ..
    $data = Json::read($file);

    # If language code not provided ..
    if (empty($code)) {
        # .. use current language
        $code = kirby()->languageCode();
    }

    return $data[$code];
}


/**
 * Date & time
 */

/**
 * Provides localized date string
 *
 * @param int $date
 * @return string
 */
function date2locale(int $date): string
{
    # Load language settings
    $data = getLanguageSettings();

    # Determine date
    return date($data['dateFormat'], $date);
}


/**
 * Provides localized time string
 *
 * @param int $time
 * @param bool $hasExtension Whether to append localized time descriptor
 * @return string
 */
function time2locale(int $date, bool $hasExtension = false): string
{
    # Load language settings
    $data = getLanguageSettings();

    # Determine time
    $time = date($data['timeFormat'], $date);

    if (empty($data['timeAffix'])) {
        return $time;
    }

    return $hasExtension
        ? sprintf('%s %s', $time, $data['timeAffix'])
        : $time
    ;
}
