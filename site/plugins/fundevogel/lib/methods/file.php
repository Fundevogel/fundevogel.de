<?php

return [
    /**
     * Creates `img` tag
     *
     * @param string $classes Custom classes
     * @param string $preset Thumbnail preset
     * @param bool $noLazy Whether image should contain lightbox
     * @param bool $noLazy Whether image should be lazyloaded
     * @param array $extra Custom HTML attributes
     * @return string
     */
    'toTag' => function (
        string $classes = '',
        string $preset = 'cover',
        bool $isLightbox = false,
        bool $noLazy = false,
        array $extra = []
    ): string {
        $image = $this->thumb($preset);
        $source = $noLazy === false ? $this->placeholderUri($image->ratio()) : $image->url();
        $title = $this->titleAttribute();
        $alt = $this->source()->isEmpty()
            ? $this->altAttribute()
            : A::join([$this->altAttribute(), t('Bildquelle') . ': ' . $this->source()], ' - ')
        ;

        $attributes = [
            'class' => $classes,
            'title' => $title,
            'alt' => $alt,
            'width' => $image->width(),
            'height' => $image->height(),
            'loading' => 'eager',
            'decoding' => 'async',
        ];

        if ($noLazy === false) {
            $lazyclass = in_array($preset, option('thumbs.blurry', []))
                ? 'lazyload blur-up'
                : 'lazyload'
            ;

            $attributes = A::update($attributes, [
                'class' => sprintf('%s %s', $classes, $lazyclass),
                'loading' => 'lazy',
                'data-src' => $image->url(),
                'data-sizes' => 'auto',
            ]);
        }

        if ($isLightbox) {
            $orientation = $this->orientation() === 'landscape' ? 'full-width' : 'full-height';
            $attributes = A::update($attributes, [
                'data-bp' => $this->thumb($orientation)->url(),
                'data-caption' => $title,
            ]);
        }

        return snippet('picture', [
            'src' => $this,
            'tag' => Html::img($source, A::update($attributes, $extra)),
            'sizes' => option('thumbs.sizes')[$preset],
            'preset' => $preset,
            'noLazy' => $noLazy,
        ], true);
    },


    /**
     * Gets PDF edition frontpage (fallback included)
     *
     * @param string $classes Custom classes
     * @return string
     */
    'getFront' => function (string $classes = ''): string
    {
        # Try using default cover ..
        $default = $this->coverImage()->toFile();

        # .. otherwise use global fallback image
        $fallback = page('lesetipps')->fallback()->toFile();

        $cover = $default ?? $fallback;

        return $cover->toTag($classes, 'lesetipps.pdf', false, true);
    },
];
