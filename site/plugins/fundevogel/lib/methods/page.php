<?php

use Kirby\Cms\File;
use Kirby\Toolkit\Html;


return [
    /**
     * Provides `h1` site title
     *
     * @return string
     */
    'getSiteTitle' => function (): string
    {
        if ($this->intendedTemplate() == 'lesetipps.article') {
            return page('lesetipps')->title();

        } elseif ($this->intendedTemplate() == 'calendar.archive') {
            # TODO: Translation!
            return t('Kalenderarchiv');

        } elseif ($this->isHomePage()) {
            return 'Fundevogel';
        }

        return $this->title();
    },


    /**
     * Checks whether page has cover image
     *
     * @return bool
     */
    'hasCover' => function (): bool
    {
        return $this->cover()->isNotEmpty();
    },


    /**
     * Fetches cover image
     *
     * @return \Kirby\Cms\File
     */
    'getCover' => function (): Kirby\Cms\File
    {
        $page = $this;

        if ($this->intendedTemplate() == 'lesetipps.article') {
            if ($this->books()->isEmpty()) {
                return page('lesetipps')->fallback()->toFile();
            }

            $page = $this->books()->toPages()->first();
        }

        $cover = $page->hasCover()
            ? $page->cover()->toFile()
            : page('lesetipps')->fallback()->toFile()
        ;

        if (!$page->cover()->exists()) {
            $cover = $page->images()->first();
        }

        return $cover;
    },


    /**
     * Creates `img` tag from book cover
     *
     * @param string $classes Custom classes
     * @param bool $noLazy Whether image should be lazyloaded
     * @return string
     */
    'getBookCover' => function (string $classes = '', bool $noLazy = false): string
    {
        $image = $this->getCover();

        $preset = $image->orientation() === 'portrait'
            ? 'lesetipps.article.cover-normal'
            : 'lesetipps.article.cover-square'
        ;

        return $image->toTag($classes, $preset, false, $noLazy);
    },


    /**
     * Creates 'more' link as `a` tag
     *
     * @param string $classes Custom classes
     * @return string
     */
    'moreLink' => function(string $classes = ''): string
    {
        $link = Html::tag('a', '→ ' . t('Weiterlesen'), [
            'href' => $this->url(),
            'class' => $classes,
        ]);

        return $link;
    },


    /**
     * Fetches source images
     *
     * @return \Kirby\Cms\Files
     */
    'getImages' => function (): Kirby\Cms\Files
    {
        return $this->images()->filterBy('extension', 'not in', ['avif', 'webp']);
    },


    /**
     * SEO & metadata
     */

    /**
     * Fetches meta image
     *
     * @return \Kirby\Cms\File
     */
    'getMetaImage' => function (): Kirby\Cms\File
    {
        # Provide fallback
        $metaImage = $this->meta_image()->toFile() ?? site()->meta_image()->toFile();

        # Use cover (if available)
        if ($this->hasCover()) {
            $metaImage = $this->cover()->toFile();
        }

        # Use cover for reading tips
        if ($this->intendedTemplate() == 'lesetipps.article') {
            $metaImage = $this->book()->toPage()->cover()->toFile();
        }

        return $metaImage;
    },


    /**
     * Fetches 'OpenGraph' image
     *
     * @return \Kirby\Cms\FileVersion
     */
    'getOpengraphImage' => function (): Kirby\Cms\FileVersion
    {
        # Provide fallback
        $opengraphImage = $this->og_image()->toFile() ?? site()->og_image()->toFile();

        # Use cover (if available)
        if ($this->hasCover()) {
            $opengraphImage = $this->cover()->toFile();
        }

        # Use cover for reading tips
        if ($this->intendedTemplate() == 'lesetipps.article') {
            $opengraphImage = $this->book()->toPage()->cover()->toFile();
        }

        return $opengraphImage->thumb([
            'width'   => 1200,
            'height'  => 630,
            'quality' => 85,
            'crop'    => true,
        ]);
    },


    /**
     * Fetches 'Twittercard' image
     *
     * @return \Kirby\Cms\FileVersion
     */
    'getTwittercardImage' => function (): Kirby\Cms\FileVersion
    {
        # Provide fallback
        $twittercardImage = $this->twitter_image()->toFile() ?? site()->twitter_image()->toFile();

        # Use cover (if available)
        if ($this->hasCover()) {
            $twittercardImage = $this->cover()->toFile();
        }

        # Use cover for reading tips
        if ($this->intendedTemplate() == 'lesetipps.article') {
            $twittercardImage = $this->book()->toPage()->cover()->toFile();
        }

        return $twittercardImage->thumb([
            'width'   => 1200,
            'height'  => 675,
            'quality' => 85,
            'crop'    => true,
        ]);
    },


    /**
     * Checks whether translation for given language exists
     *
     * @param string $language Language code
     * @return bool
     */
    'isTranslated' => function (string $language): bool
    {
        # Since archive is created as virtual page ..
        if ($this->intendedTemplate() == 'calendar.archive') {
            # .. it is always 'translated'
            return true;
        }

        # Check if translation file for given language exists
        return $this->translation($language)->exists();
    },


    /**
     * Checks whether page has any translations
     *
     * @return bool
     */
    'hasTranslations' => function (): bool
    {
        $languages = kirby()->languages()->not(kirby()->defaultLanguage()->code());

        foreach ($languages as $language) {
            if ($this->isTranslated($language)) {
                return true;
            }
        }

        return false;
    },


    /**
     * Checks whether page has translated siblings
     *
     * @return bool
     */
    'hasTranslatedSiblings' => function (): bool
    {
        if ($this->hasPrevListed() && $this->prevListed()->isTranslated(kirby()->languageCode())) {
            return true;
        }

        if ($this->hasNextListed() && $this->nextListed()->isTranslated(kirby()->languageCode())) {
            return true;
        }

        return false;
    },


    /**
     * Fetches previous translated sibling
     *
     * @return \Kirby\Cms\Page|null
     */
    'prevTranslated' => function ()
    {
        return $this->prevAll()->listed()->onlyTranslated()->last();
    },


    /**
     * Fetches next translated sibling
     *
     * @return \Kirby\Cms\Page|null
     */
    'nextTranslated' => function ()
    {
        return $this->nextAll()->listed()->onlyTranslated()->first();
    },
];
