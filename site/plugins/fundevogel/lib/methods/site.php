<?php

use Kirby\Filesystem\F;

use Bnomei\Fingerprint;


return [
    /**
     * Includes for `head` tag
     */

    /**
     * Creates CSS tag for `<head>`
     *
     * @return string
     */
    'css' => function (): string
    {
        # When in production ..
        if (option('environment') == 'production') {
            # (1) .. load minified CSS
            $css = F::read(sprintf('%s/css/main.css', kirby()->root('assets')));

            # (2) .. provide `style` tag along with it
            return sprintf('<style nonce="%s">%s</style>', site()->nonce(), $css);
        }

        # .. otherwise, provide `link` tag & unminified CSS file
        return Fingerprint::css('assets/css/main.css', ['integrity' => true]);
    },


    /**
     * Creates JS tag for `<head>`
     *
     * @return string
     */
    'js' => function (): string
    {
        # Serve JS file
        return Fingerprint::js('assets/js/main.js', ['integrity' => true, 'defer' => true]);
    },
];
