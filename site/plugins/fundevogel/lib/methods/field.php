<?php

return [
    /**
     * Generic helpers
     */

    /**
     * Adds entry to structure field
     *
     * @param Kirby\Cms\Field $field
     * @param array $entry Entry to added
     * @return void
     */
    'addToStructure' => function (Kirby\Cms\Field $field, array $entry): void
    {
        $value = $field->yaml();

        if (isset($entry[0]) && is_array($entry[0])) {
            array_push($value, ...$entry);

        } else {
            array_push($value, $entry);
        }

        $field->parent()->update([
            $field->key() => Yaml::encode($value)
        ]);
    },


    /**
     * Strips non-numeric characters (for use with 'tel:' inside `a` tag)
     *
     * @param Kirby\Cms\Field $field
     * @return string
     */
    'string2tel' => function (Kirby\Cms\Field $field): string
    {
        return preg_replace('![^0-9\+]+!', '', $field->value());
    },


    /**
     * Date & time
     */

    /**
     * Provides localized date string
     *
     * @param Kirby\Cms\Field $field
     * @return string
     */
    'date2locale' => function (Kirby\Cms\Field $field): string
    {
        # Parse date
        $date = strtotime($field->value());

        return date2locale($date);
    },


    /**
     * Provides localized time string
     *
     * @param Kirby\Cms\Field $field
     * @param bool $hasExtension Whether to append localized time descriptor
     * @return string
     */
    'time2locale' => function (Kirby\Cms\Field $field, bool $hasExtension = false): string
    {
        # Parse time
        $time = strtotime($field->value());

        return time2locale($time, $hasExtension);
    },
];
