<?php

use Kirby\Cms\KirbyTag;
use Kirby\Http\Url;
use Kirby\Toolkit\A;
use Kirby\Toolkit\Str;
use Kirby\Toolkit\Html;


return [
    /**
     * Core extensions
     */
    'link' => [
        # Default attributes:
        # - class
        # - lang
        # - rel
        # - role
        # - target
        # - text
        # - title
        'attr' => A::merge(KirbyTag::$types['link']['attr'], [
            'color',
        ]),


        /**
         * Creates `a` tag
         *
         * @param Kirby\Cms\KirbyTag $tag
         * @return string
         */
        'html' => function(Kirby\Cms\KirbyTag $tag): string
        {
            # Check if target is Wikipedia article
            if (Str::startsWith($tag->value, 'wiki')) {
                # Determine article name
                $article = $tag->text;

                if (Str::contains($tag->value, '=')) {
                    $article = Str::split($tag->value, '=')[1];
                }

                # Determine `title` attribute
                if (empty($tag->title) === true) {
                    $tag->title = sprintf('\'%s\' @ Wikipedia', $article);
                }

                $lang = empty($tag->lang) === false ? $tag->lang : 'de';

                # Set `href` attribute
                $tag->value = Str::replace('https://' . $lang . '.wikipedia.org/wiki/' . $article, ' ', '_');

            # .. otherwise, default Kirby stuff
            } else {
                if (empty($tag->lang) === false) {
                    $tag->value = Url::to($tag->value, $tag->lang);
                }
            }

            # Check if URL is external
            if (is_null(page($tag->value))) {
                $tag->target = 'blank';
            }

            # Determine attributes
            # (1) Kirby defaults
            $attributes = [
                'rel'    => $tag->rel,
                'class'  => $tag->class,
                'role'   => $tag->role,
                'title'  => $tag->title,
                'target' => $tag->target,

                # (2) Prevent Barba preloading
                'data-barba-prevent' => 'true',
            ];

            # (2) Add tooltips if `title` attribute present
            if (empty($tag->title) === false) {
                # Determine base color
                if (in_array($tag->color, ['red', 'orange']) === false) {
                    $tag->color = 'red';
                }

                # Add proper classes & theme
                $attributes = A::merge($attributes, [
                    'class' => Str::replace('js-tippy font-normal text-%s-medium hover:text-%s-dark', '%s', $tag->color),
                    'data-tippy-theme' => 'fundevogel ' . $tag->color,
                ]);
            }

            return Html::a($tag->value, $tag->text, $attributes);
        },
    ],


    /**
     * Custom tags
     */
    'page' => [
        /**
         * Creates localized date string for 'modified'
         *
         * @param Kirby\Cms\KirbyTag $tag
         * @throws Exception
         * @return string
         */
        'html' => function(Kirby\Cms\KirbyTag $tag): string
        {
            if ($tag->value === 'modified') {
                return date2locale($tag->parent()->modified());
            }

            throw new Exception(sprintf('Invalid argument: "%s"', $tag->value));
        },
    ],
    'short' => [
        'attr' => [
            'desc',
            'color',
        ],


        /**
         * Creates `abbr` tag containing short description
         *
         * @param Kirby\Cms\KirbyTag $tag
         * @return string
         */
        'html' => function(Kirby\Cms\KirbyTag $tag): string
        {
            # Determine base color
            $color = in_array($tag->color, ['red', 'orange']) === true
                ? $tag->color
                : 'red'
            ;

            return Html::tag('abbr', $tag->value, [
                'class' => sprintf('js-tippy font-normal text-%s-medium hover:text-%s-dark border-b-2 border-dashed border-%s-medium hover:border-%s-dark cursor-help', $color, $color, $color, $color),
                'data-tippy-theme' => 'fundevogel ' . $color,
                'title' => $tag->desc,
            ]);
        },
    ],
    'quote' => [
        'attr' => [
            'author',
            'color',
            'border',
        ],


        /**
         * Creates `blockquote` tag (with optional `cite` tag containing author)
         *
         * @param Kirby\Cms\KirbyTag $tag
         * @return string
         */
        'html' => function(Kirby\Cms\KirbyTag $tag): string
        {
            # Determine base color
            $color = in_array($tag->color, ['red', 'orange']) === true
                ? $tag->color
                : 'red'
            ;

            $data = [
                'text' => $tag->value,
                'author' => $tag->author,
                'color' => $color,
            ];

            return snippet('base/quote', $data, true);
        },
    ],
    'pubkey' => [
        'attr' => [
            'fingerprint',
            'text',
        ],


        /**
         * Creates `a` tag linking GPG key
         *
         * @param Kirby\Cms\KirbyTag $tag
         * @return string
         */
        'html' => function(Kirby\Cms\KirbyTag $tag): string
        {
            if ($pgpKey = site()->file($tag->value . '.asc')) {
                if ($tag->fingerprint) {
                    return $pgpKey->fpr();
                }

                return kirbytag([
                    'link' => $pgpKey->url(),
                    'class' => 'js-tippy outline-none',
                    'text' => $tag->text ?? 'Public PGP-Key',
                    'title' => $pgpKey->crypto() . ' (' . $pgpKey->algorithm() . ') - ' . $pgpKey->length() . 'bit',
                ]);
            }

            return '';
        },
    ],
];
