<?php

use Kirby\Cms\App as Kirby;


# Load helper functions
require_once __DIR__ . '/lib/functions.php';

# Load page & file models
require_once __DIR__ . '/lib/models.php';


# Initialize plugin
Kirby::plugin('fundevogel/core', [
    # Blueprints
    # See https://getkirby.com/docs/reference/plugins/extensions/blueprints
    'blueprints' => [
        'blocks/books' => __DIR__ . '/blocks/books.yml',
    ],

    # Methods
    # (1) Field methods
    # See https://getkirby.com/docs/reference/plugins/extensions/field-methods
    'fieldMethods' => require_once __DIR__ . '/lib/methods/field.php',

    # (2) File methods
    # See https://getkirby.com/docs/reference/plugins/extensions/file-methods
    'fileMethods' => require_once __DIR__ . '/lib/methods/file.php',

    # (3) Page methods
    # See https://getkirby.com/docs/reference/plugins/extensions/page-methods
    'pageMethods' => require_once __DIR__ . '/lib/methods/page.php',

    # (4) Site methods
    # See https://getkirby.com/docs/reference/plugins/extensions/site-methods
    'siteMethods' => require_once __DIR__ . '/lib/methods/site.php',

    # Page models
    # See https://getkirby.com/docs/reference/plugins/extensions/page-models
    'pageModels' => [
        'calendar.event'  => 'iCalPage',
        'calendar.events' => 'iCalPage',
    ],

    # Snippets
    # See https://getkirby.com/docs/reference/plugins/extensions/snippets
    'snippets' => [
        # (1) Generic components
        'picture' => __DIR__ . '/snippets/picture.php',

        # (2) Template specific
        'calendar/quickview' => __DIR__ . '/snippets/custom/quickview.php',
        'tech/list'  => __DIR__ . '/snippets/custom/packages.php',

        # (3) Blocks
        'blocks/books' => __DIR__ . '/blocks/books.php',
        'blocks/hr'    => __DIR__ . '/blocks/hr.php',
    ],

    # KirbyTags
    # See https://getkirby.com/docs/reference/plugins/extensions/kirbytags
    'tags' => require_once __DIR__ . '/lib/tags.php',
]);
