<?php

return function ($page) {
    $data = [
        'data' => $page,
        'verdict' => $page->verdict(),
        'useTaxonomy' => true,
        'useDetails' => true,
    ];

    return [
        'book' => $page->book()->toPage(),
        'data' => $data,
        'layouts' => $page->layouts()->toLayouts(),
    ];
};
