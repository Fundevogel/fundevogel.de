<?php

return function ($page) {
    return [
        'dossier' => $page->file('dossier.pdf'),
        'gallery' => $page->gallery()->toFiles(),
        'layouts' => $page->layouts()->toLayouts(),
    ];
};
