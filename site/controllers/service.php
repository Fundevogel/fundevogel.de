<?php

return function ($page) {
    return [
        'cards' => $page->cards()->toStructure(),
        'heading' => t('Service-Überschrift'),
    ];
};
