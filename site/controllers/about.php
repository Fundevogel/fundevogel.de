<?php

return function ($page) {
    return [
        'caption' => $page->caption()->html(),
        'images' => $page->gallery()->toFiles(),
        'preset' => 'about.cover',
    ];
};
