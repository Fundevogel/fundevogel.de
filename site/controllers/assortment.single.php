<?php

return function ($page) {
    $data = [
        'heading' => t('Auswahl unserer Lieblinge'),
        'data' => $page->favorites()->toStructure(),
        'icon' => 'book-closed-filled',
    ];

    return [
        'data' => $data,
        'layouts' => $page->layouts()->toLayouts(),
    ];
};
