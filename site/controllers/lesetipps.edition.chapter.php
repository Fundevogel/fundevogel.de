<?php

return function ($page) {
    return [
        'books' => $page->children()->listed(),
    ];
};
