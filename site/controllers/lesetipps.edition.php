<?php

return function ($page) {
    return [
        'chapters' => $page->children()->listed(),
    ];
};
