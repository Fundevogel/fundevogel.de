<?php

return function ($kirby) {
    return [
        'editions' => $kirby->collection('bibliolists/files')->flip(),
    ];
};
