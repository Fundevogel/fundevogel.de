<?php

return function ($page) {
    return [
        'team' => $page->team()->toStructure(),
    ];
};
