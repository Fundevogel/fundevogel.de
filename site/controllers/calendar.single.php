<?php

return function ($page) {
    return [
        'layouts' => $page->layouts()->toLayouts(),
        'siblings' => $page->siblings(false)->filterBy('intendedTemplate', 'calendar.single'),
    ];
};
