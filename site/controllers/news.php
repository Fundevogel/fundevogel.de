<?php



return function ($page) {
    $perPage = $page->perpage()->int();

    $templates = [
        'news.toot',
        'news.event',
        'news.article',
        'news.recommendation',
    ];

    $newsTotal = $page->children()->listed()->filterBy('intendedTemplate', 'in', $templates)->flip();
    $newsPerPage = $newsTotal->paginate(($perPage >= 1) ? $perPage : 5);

    return [
        'articleLast' => $newsTotal->last(),
        'news' => $newsPerPage,
        'nothingLeft' => Kirby\Toolkit\Html::tag('h3', t('Mehr haben wir nicht!'), ['class' => 'text-center']),
        'pagination' => $newsPerPage->pagination(),
    ];
};
