<?php

return function ($kirby) {
    # Filter by categories indicating a closed event
    return $kirby->collection('events/current')->filter(function ($event) {
        $closedEvents = [
            'Schulinterne Veranstaltung',
            'Kindergarteninterne Veranstaltung',
            'Veranstaltung für angemeldete Schulklassen'
        ];

        if (in_array($event->category(), $closedEvents) === true) {
            return $event;
        }
    });
};
