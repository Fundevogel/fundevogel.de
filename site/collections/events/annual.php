<?php

return function ($site) {
    return $site->find('kalender')->children()->listed()
                ->filterBy('intendedTemplate', 'calendar.single');
};
