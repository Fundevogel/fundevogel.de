<?php

return function ($site) {
    return $site->find('aktuelles')->children()->listed()->flip()
                ->filterBy('intendedTemplate', 'news.article');
};
