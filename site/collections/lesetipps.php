<?php

return function ($site) {
    return $site->find('lesetipps')->children()->listed()->flip()
                ->filterBy('intendedTemplate', 'lesetipps.article');
};
