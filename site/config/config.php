<?php

use Kirby\Cms\App;

# Load credentials & secrets
Beebmx\KirbyEnv::load(__DIR__ . '/../..');


/**
 * General settings
 */

return [
    /**
     * Includes
     */

    # Cache
    'cache' => require __DIR__ . '/core/cache.php',

    # Routes
    'routes' => require __DIR__ . '/core/routes.php',

    # Hooks
    'hooks' => require __DIR__ . '/core/hooks.php',

    # Thumbnails
    'thumbs' => require __DIR__ . '/core/thumbs.php',


    /**
     * Kirby options
     */

    # Send emails from contact form
    'email' => [
        'transport' => [
            'type' => 'smtp',
            'host' => env('mail_server'),
            'security' => true,
            'port' => 587,
            'auth' => true,
            'username' => env('mail_username'),
            'password' => env('mail_password'),
        ],
    ],

    # Set alternative `home` identifier
    'home' => 'aktuelles',

    # Enable languages (currently supported are DE, EN & FR)
    'languages' => true,

    # Sitemap settings
    'sitemap.ignore' => [
        'error',
        'kontakt/vielen-dank',
    ],

    # Typography settings
    # See https://getkirby.com/docs/reference/system/options/smartypants
    'smartypants' => true,

    # Disable UUIDs
    'content.uuid' => false,


    /**
     * Plugin options
     */

    # Enable auto-linking legal norms (specific templates only)
    'kirby3-gesetze.drivers.blockList' => ['dejure', 'buzer'],
    'kirby3-gesetze.allowList' => ['default'],
    'kirby3-gesetze.attributes' => [
        'class' => 'js-tippy',
        'data-tippy-theme' => 'fundevogel red',
    ],

    # Define AVIF conversion options
    'fundevogel.colorist' => [
        'formats' => ['avif', 'webp'],
        'tonemap' => false,
        'speed' => 0,
        'yuv' => '420',
    ],

    # Store redirects as JSON
    'distantnative.retour.config' => __DIR__ . '/extras/redirects.json',

    # Markdown field settings
    # See https://github.com/sylvainjule/kirby-markdown-field
    'community.markdown-field' => [
        'buttons' => [
            'headlines', 'bold', 'italic', 'ul','blockquote',
            'divider',
            'link', 'email', 'pagelink', 'file',
        ],
        'font' => [
            'family'  => 'sans-serif',
            'scaling' => true,
        ],
    ],

    # Utilize manifest file with hashed assets
    'ready' => function (App $kirby) {
        return [
            'bnomei.fingerprint.query' => $kirby->root('assets') . '/manifest.json',
        ];
    },

    # Include janitor jobs
    'bnomei.janitor.jobs' => include_once __DIR__ . '/extras/jobs.php',

    'bnomei.securityheaders' => [
        # Disable CSP rules on the panel,
        # force everywhere else (development as well as production)
        'enabled' => function () {
            # Panel check, borrowed from @bnomei's `security-headers`
            # See https://github.com/steirico/kirby-plugin-custom-add-fields/issues/37
            $isPanel = strpos(
                kirby()->request()->url()->toString(),
                kirby()->urls()->panel
            ) !== false;

            if ($isPanel) {
                return false;
            }

            // return 'force';
            # Disabling CSP for now
            return false;
        },
        # Disable security headers (see `.htaccess`)
        'headers' => [],
        'loader' => function () {
            return kirby()->root('config') . '/extras/csp.json';
        },
    ],
];
