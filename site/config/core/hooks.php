<?php

return [
    /**
     * Block editiing of past events
     *
     * @param Kirby\Cms\Page $page
     * @return void
     */
    'page.update:before' => function (Kirby\Cms\Page $page) {
        if (kirby()->collection('events/past')->has($page)) {
            throw new Exception('Vergangene Veranstaltungen sind gesperrt.');
        }
    },


    /**
     * Modify text being processed with `kt`
     *
     * @param string $text Processed kirbytext
     * @return string
     */
    'kirbytext:after' => function (string $text): string {
        # Modify 'author' inside `blockquote` elements for consistent styling
        $text = preg_replace_callback('#<footer>(.*?)</footer>#', function ($matches) {
            $html = '';

            # Create updated `cite` tag
            $html .= '<footer>';
            $html .= useSVG(t('Zitat'), 'inline w-6 h-6 -mt-1 mr-1 text-red-medium fill-current', 'message-filled');
            $html .= '<span class="text-sm text-red-medium not-italic font-normal">' . $matches[1] . '</span>';
            $html .= '</footer>';

            return $html;
        }, $text);


        # Add class to paragraph & list elements
        # See https://forum.getkirby.com/t/add-classes-to-textarea-field-output/14060/5

        $from = [];
        $from[0] = '/<p>/';
        $from[1] = '/<ul>/';
        $from[2] = '/<ol>/';

        $to = [];
        $to[0] = '<p class="content">';
        $to[1] = '<ul class="list">';
        $to[2] = '<ol class="list">';

        return preg_replace($from, $to, $text);
    },
];
