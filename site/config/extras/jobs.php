<?php

use Kirby\Cms\File;
use Kirby\Cms\Page;
use Kirby\Data\Yaml;
use Kirby\Filesystem\F;
use Kirby\Toolkit\Str;

use Fundevogel\Chart;
use Fundevogel\Mastodon\Api as Mastodon;
use Fundevogel\Pcbis\Utilities\Butler;

return [
    /**
     * Jobs for 'book' page objects
     */

    /**
     * Upgrades 'book' page object
     *
     * @param \Kirby\Cms\Page $page
     * @param string $data Page ID
     * @return array Job status
     */
    'upgradeBook' => function(Page $page, string $data): array {
        # Define fallback page for drafts
        if ($page === null) {
            $page = site()->index(true)->findByID($data);
        }

        try {
            $success = $page->upgradeBook();
            $page->updateOla();
        } catch (Exception $e) {
            return [
                'status' => 404,
                'label'  => $e->getMessage(),
                'reload' => false,
            ];
        }

        return [
            'status' => $success ? 200 : 404,
            'label'  => $success ? 'Upgrade erfolgreich!' : 'Upgrade fehlgeschlagen!',
            'reload' => $success,
        ];
    },


    /**
     * Updates OLA within 'book' page object
     *
     * @param \Kirby\Cms\Page $page
     * @param string $data Page ID
     * @return array Job status
     */
    'ola' => function(Page $page, string $data): array {
        # Define fallback page for drafts
        if ($page === null) {
            $page = site()->index(true)->findByID($data);
        }

        # Update page
        $success = (bool) $page->updateOla();

        return [
            'status' => $success ? 200 : 404,
            'label'  => $success ? 'Update erfolgreich!' : 'Update fehlgeschlagen!',
            'reload' => $success,
        ];
    },


    /**
     * Loads data for 'book' page object
     *
     * @param \Kirby\Cms\Page $page
     * @param string $data Page ID
     * @return array Job status
     */
    'loadBook' => function(Page $page, string $data): array {
        # Define fallback page for drafts
        if ($page === null) {
            $page = site()->index(true)->findByID($data);
        }

        # Update page
        $success = $page->updateBook();

        return [
            'status' => $success ? 200 : 404,
            'label'  => $success ? 'Update erfolgreich!' : 'Update fehlgeschlagen!',
            'reload' => $success,
        ];
    },


    /**
     * Downloads cover image & adds metadata
     *
     * @param \Kirby\Cms\Page $page
     * @param string $data Page ID
     * @return array Job status
     */
    'downloadCover' => function(Page $page, string $data): array {
        # Define fallback page for drafts
        if ($page === null) {
            $page = site()->index(true)->findByID($data);
        }

        # If ISBN not available ..
        if ($page->isbn()->isEmpty()) {
            # .. fail early
            return [
                'status' => 404,
                'label' => 'Keine ISBN verfügbar!',
                'reload' => false,
            ];
        }

        # Define filename
        $filename = Str::slug($page->title());

        if ($page->author()->isNotEmpty()) {
            $filename = sprintf('%s_%s', $filename, Str::slug($page->author()));
        }

        $file = new File([
            'parent' => $page,
            'filename' => $filename . '.jpg',
        ]);

        if (!$file->exists()) {
            Butler::downloadCover($page->isbn()->value(), $file->root());
        }

        try {
            $book = $page->toBook();

            $file->update([
                'titleAttribute' => '"' . $book->title() . '" von ' . $book->author(),
                'source' => 'Deutsche Nationalbibliothek',
                'altAttribute' => 'Cover des Buches "' . $book->title() . '" von ' . $book->author(),
                'template' => 'image',
            ]);
        } catch (Exception $e) {
        }

        try {
            $page->update([
                'cover' => Yaml::encode($file->filename()),
            ]);
        } catch (Exception $e) {
            return [
                'status' => 404,
                'label' => $e->getMessage(),
                'reload' => false,
            ];
        }

        return [
            'status' => 200,
            'label' => 'Update erfolgreich!',
            'reload' => true,
        ];
    },


    /**
     * Jobs for 'edition' file objects
     */

    /**
     * Creates metadata for file object
     *
     * @param \Kirby\Cms\Page $page
     * @param string $data Filename
     * @return array Job status
     */
    'createMetadata' => function(Page $page, string $data) {
        # Check if PDF file exists
        if ($file = $page->file($data)) {
            # Build data for PDF
            $extension = 'jpg';
            $inputFile = $file->root();
            $outputFile = $file->root() . '.' . $extension;

            # (1) Generate metadata
            $fileName = basename($outputFile);
            preg_match('/[0-9]{4}/', $fileName, $year);
            $season = Str::contains($fileName, 'fruehjahr') ? 'Frühjahr' : 'Herbst';

            $cover = $page->images($fileName);

            # (2) Create thumbnail image ..
            if (!F::exists($outputFile) || (F::modified($outputFile) < $file->modified())) {
                # .. only if it doesn't exist or PDF file changed since its creation
                $im = new Imagick();
                $im->setResolution(300, 300);
                $im->readImage($inputFile . '[0]');
                // $im->setImageBackgroundColor('white');
                $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
                $im->setImageFormat($extension);
                $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                $im->writeImage($outputFile);
                $im->clear();

                # Update thumbnail image metadata
                $cover = new File([
                    'source' => $outputFile,
                    'filename' => $fileName,
                    'parent' => $file->parent(),
                ]);
            }

            $cover->update([
                'template' => 'image',
                'titleAttribute' => 'Unsere Empfehlungen im ' . $season,
                'altAttribute' => 'Coverbild unserer Empfehlungen im ' . $season,
                'source' => 'Eigenmaterial',
                'caption_wanted' => true,
                'caption' => $season,
            ]);

            $file->update([
                'coverImage' => $fileName,
                'edition' => $season,
                'year' => $year[0],
            ]);
        }

        return [
            'status' => $file ? 200 : 404,
            'label' => $file ? 'Erfolgreich!' : 'Mistikus totalus!',
            'reload' => $file ? true : false,
        ];
    },


    /**
     * Jobs for 'dependency' page object
     */

    /**
     * Fetches website statistics from third-party providers
     *
     * @param \Kirby\Cms\Page $page
     * @return array Job status
     */
    'fetchStatistics' => function(Page $page): array {
        $source = [];

        # Fetch statistics
        $toolsetDir = kirby()->root('base') . '/lib/toolset';

        # (1) Lines of code
        exec(sprintf('bash %s/lines_of_code.bash', $toolsetDir), $outputLines);

        if (is_array($outputLines) && count($outputLines) >= 1) {
            $source['loc'] = number_format((string) $outputLines[0], 0, ',', '.');
        }

        # (2) Average commits/month for last three months
        exec(sprintf('bash %s/average_commits_per_month.bash', $toolsetDir), $outputCommits);

        if (is_array($outputCommits) && count($outputCommits) >= 1) {
            $source['commits'] = (string) $outputCommits[0];
        }

        # (3) PageSpeed performance score
        if ($score = $page->pageSpeed()) {
            $source['pagespeed'] = $score;
        }

        # (4) Observatory security grade
        if ($grade = $page->observatory()) {
            $source['observatory'] = $grade;
        }

        # Report back results
        $success = true;
        $message = 'Update erfolgreich!';

        if (empty($source)) {
            $message = 'Nichts zu tun!';
        } else {
            try {
                # Attempt page update
                $page->update($source);
            } catch (Exception $e) {
                # Save error message
                $message = $e->getMessage();
                $success = false;
            }
        }

        return [
            'status' => $success ? 200 : 404,
            'label' => $message,
            'reload' => $success,
        ];
    },


    /**
     * Fetches language statistics from third-party providers
     *
     * @param \Kirby\Cms\Page $page
     * @return array Job status
     */
    'fetchLanguages' => function(Page $page): array {
        # If something goes wrong ..
        if (empty($langData = $page->fetchLangData())) {
            # .. fail early
            return [
                'status' => 404,
                'label' => 'Mistikus totalus!',
                'reload' => false,
            ];
        }

        # (1) Extract languages
        $languages = array_keys($langData);

        # (2) Extract language shares
        $numbers = array_values($langData);
        $total = array_sum($numbers);

        # Fetch language colors
        $colorData = json_decode(F::read(__DIR__ . '/colors.json'), true);

        $data = [];

        # Combine data sources
        for ($i = 0; $i < count($langData); $i++) {
            $data[$i] = [
                'title' => $languages[$i],
                'share' => ($numbers[$i] * 100) / $total,
                'color' => $colorData[$languages[$i]],
            ];
        }

        $file = new File([
            'parent' => $page,
            'filename' => 'programmiersprachen.svg',
            'template' => 'image'
        ]);

        # Create chart
        $chart = new Chart($data, 1);

        # Attempt disk write ..
        if (!F::write($file->root(), $chart->render('DonutGraph', [
            'svg_class' => 'block w-56 h-56',
            'donut_slice_gap' => 1.5,
            'inner_radius' => 0.7,
            'start_angle'  => -90,
            'stroke_width' => 0,
        ]))) {
            # .. otherwise fail
            throw new \Exception('Couldn\'t create chart!');
        }

        try {
            $file->update([
                'altAttribute' => 'Abbildung verwendeter Programmiersprachen als Ringdiagramm',
                'titleAttribute' => 'Mehr als nur HTML - die Webseite des Fundevogels',
                'source' => 'Eigenmaterial',
            ]);

            $page->update([
                'langData' => Yaml::encode($chart->data),
                'chart' => Yaml::encode($file->filename()),
            ]);
        } catch (Exception $e) {
            return [
                'status' => 404,
                'label' => $e->getMessage(),
                'reload' => false,
            ];
        }

        return [
            'status' => 200,
            'label' => 'Update erfolgreich!',
            'reload' => true,
        ];
    },


    /**
     * Fetches package information from third-party providers
     *
     * @param \Kirby\Cms\Page $page
     * @return array Job status
     */
    'fetchPackages' => function(Page $page): array {
        # Attempt to ..
        try {
            # .. update package data
            $page->update([
                'phpData' => Yaml::encode($page->phpData()),
                'pkgData' => Yaml::encode($page->pkgData()),
            ]);
        } catch (Exception $e) {
            return [
                'status' => 404,
                'label' => $e->getMessage(),
                'reload' => false,
            ];
        }

        return [
            'status' => 200,
            'label' => 'Update erfolgreich!',
            'reload' => true,
        ];
    },


    /**
     * Tools third-party APIs
     */

    /**
     * Fetches 'Toot' from Mastodon's REST API
     *
     * @param \Kirby\Cms\Page $page
     * @param string $data Page ID
     * @return array Job status
     */
    'fetchToot' => function(Page $page, string $data): array {
        # Define fallback page for drafts
        if ($page === null) {
            $page = site()->index(true)->findByID($data);
        }

        $success = false;

        if ($id = $page->toot()->value()) {
            # Create API object
            $api = new Mastodon('freiburg.social');

            # Assign access token
            $api->accessToken = env('access_token');

            # Fetch toot
            $toot = $api->statuses()->get($id);

            # Update page
            $page->update([
                'tootText' => $toot->content(),
                'tootDate' => $toot->createdAt(),
                'tootLink' => $toot->url(),
            ]);

            # Download images
            foreach ($toot->downloadMedia($page->root()) as $index => $file) {
                # Build path
                $name = basename($file);
                $path = $page->root() . '/' . $name;

                # Update file
                $file = new File([
                    'filename' => basename($path),
                    'parent' => $page,
                ]);

                $file->update([
                    'template' => 'image',
                    'description' => $toot->data['media_attachments'][$index]['description'],
                ]);
            }

            $success = true;
        }

        return [
            'status' => $success ? 200 : 404,
            'label'  => $success ? 'Update erfolgreich!' : 'Mistikus totalus!',
            'reload' => $success,
        ];
    },
];
