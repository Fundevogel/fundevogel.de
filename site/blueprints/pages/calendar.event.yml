title:
  de: Veranstaltung
  en: Event
icon: calendar

num: "{{ page.date.toDate('Ymd') }}"

options: options/post

tabs:
  tab_content:
    extends: tabs/content
    fields:
      text:
        type: markdown
        help:
          de: |
            Dieser Text ist wird unter der Rubrik "Aktuelles" angezeigt.
          en: |
            This text will be displayed on the "News" page.

      details:
        type: markdown
        help:
          de: |
            Dieser Text ist optional und wird <span style="text-decoration: underline">nicht</span> in der Rubrik "Aktuelles" angezeigt.
          en: |
            This text is optional and <span style="text-decoration: underline">will not</span> be displayed on the "News" page.

    sidebar:
      sidebar_fields:
        type: fields
        fields:
          date:
            extends: fields/base/date
            label:
              de: (Erster) Tag
              en: (First) day of the event
            time:
              step: 5
            width: 1/2

          dateEnd:
            extends: fields/base/date
            label:
              de: Letzter Tag
              en: Last day of the event
            default: false
            time:
              step: 5
            width: 1/2

          category:
            extends: fields/base/select
            label:
              de: Art der Veranstaltung
              en: Event category
            options:
              # Author
              Autorengespräch: Autorengespräch
              Lesung: Lesung

              # School + Kindergarten
              "Kindergarteninterne Veranstaltung": Kindergarten
              "Schulinterne Veranstaltung": Schule
              "Veranstaltung für angemeldete Schulklassen": Schulklassen

              # Generic
              Büchertisch: Büchertisch
              "Feier / Fest": Feier bzw. Fest
              Weltgeschichtentag: Weltgeschichtentag
              "Präsentation und Vorstellung ausgewählter Neuerscheinungen": HerbstLESE
              "Lirum Larum Lesefest": LirumLarum

          audience:
            label:
              de: Zielgruppe
              en: Audience
            type: text
            help:
              de: |
                Beispiel: "Für Kinder ab 6 Jahren"
              en: |
                Example: "for children from 6 years onwards"

          location:
            label:
              de: Veranstaltungsort
              en: Event location
            type: text
            icon: map

          coordinates:
            label:
              de: Koordinaten
              en: Coordinates
            type: locator
            tiles: openstreetmap
            center:
              lat: 47.992697
              lon: 7.852519
            zoom:
              min: 4
              default: 18
              max: 18
            marker: light

  tab_registration:
    label:
      de: Anmeldung
      en: Registration
    icon: users
    columns:
      left_column:
        width: 1/2
        sections:
          left_fields:
            type: fields
            fields:
              registrationRequired:
                extends: fields/base/toggle
                label:
                  de: Anmeldung notwendig?
                  en: Registration required?
                default: false
                width: 1/3

              openUntil:
                extends: fields/base/date
                label:
                  de: .. bis?
                  en: .. by?
                width: 1/3
                when:
                  registrationRequired: true

              seats:
                extends: fields/base/number
                label:
                  de: Verfügbare Plätze
                  en: Available seats
                width: 1/3
                after:
                  de: Plätze
                  en: seats
                when:
                  registrationRequired: true

              infos:
                extends: fields/base/checkboxes
                label:
                  de: Benötigte Informationen
                  en: Information needed
                options:
                  - Name
                  - Email
                  - Telefon
                  - Personenzahl
                width: 1/2
                when:
                  registrationRequired: true

              contact:
                extends: fields/base/checkboxes
                label:
                  de: Kontaktmöglichkeiten
                  en: Contact details
                options:
                  - im Laden
                  - via Email
                  - telefonisch
                  - per Telefax
                width: 1/2
                when:
                  registrationRequired: true

              note:
                label:
                  de: Freitext
                  en: Free text
                type: markdown
                help:
                  de: |
                    Dieser Text ist optional und wird unter den Angaben zur Anmeldung angezeigt.
                  en: |
                    This text is optional and will be shown below the information regarding registration.
                when:
                  registrationRequired: true

      right_column:
        width: 1/2
        sections:
          right_fields:
            type: fields
            fields:
              link: fields/base/url

              costsAdmission:
                extends: fields/base/toggle
                label:
                  de: Eintrittspreis?
                  en: Admission fee?
                default: false
                width: 1/3

              admissionChildren:
                extends: fields/base/number
                label:
                  de: Kinder
                  en: Children
                step: .01
                after: €
                width: 1/3
                when:
                  costsAdmission: true

              admissionAdults:
                extends: fields/base/number
                label:
                  de: Erwachsene
                  en: Adults
                step: .01
                after: €
                width: 1/3
                when:
                  costsAdmission: true
