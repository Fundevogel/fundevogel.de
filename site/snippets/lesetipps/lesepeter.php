<h3 class="mb-4 underline"><?= t('Über LesePeter') ?></h3>
<div class="flex items-center">
    <div class="flex-1">
        <?= (new Field(null, 'desc', $book->getAward()['description']))->kirbytext() ?>
    </div>
    <div class="flex-none">
        <?php
            # Fetch image
            $image = new File([
                'parent' => $site,
                'filename' => 'lesepeter.png',
            ]);

            # Create `img` element
            echo $image->toTag('js-tippy ml-6 w-auto h-48 lg:h-64 hidden md:block cursor-help', 'lesepeter.mascot', false, true, $options = [
                'title' => 'Daumen hoch für gute Bücher!',
                'alt'=> 'LesePeter-Logo',
                'data-tippy-theme' => 'fundevogel red',
            ]);
        ?>
    </div>
</div>
