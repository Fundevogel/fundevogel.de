<?php snippet('header') ?>

<?php slot('header') ?>
<header class="container">
    <div class="flex flex-col lg:flex-row">
        <div class="flex-1">
            <?= $page->text()->kt() ?>
        </div>
        <div class="mt-12 flex-none flex justify-center items-center">
            <div class="lg:ml-12">
                <?php snippet('base/lightbox', compact('images', 'caption')) ?>
            </div>
        </div>
    </div>
</header>
<?php endslot() ?>

<?php if ($body = $slots->body()) echo $body; ?>

<?php if ($footer = $slots->footer()) echo $footer; ?>

<?php snippet('footer') ?>
