<?php snippet('header') ?>

<?php slot('header') ?>
<header class="container">
    <div class="flex flex-col lg:flex-row">
        <div class="flex-1">
            <?= $page->text()->kt() ?>
        </div>
        <div class="mt-12 flex-none text-center">
            <?php snippet('cover') ?>
        </div>
    </div>
</header>
<?php endslot() ?>

<?php if ($body = $slots->body()) echo $body; ?>

<?php slot('footer') ?>
<hr>
<section class="container">
    <h2 class="mb-12 text-center"><?= $heading ?></h2>
    <div class="js-masonry">
        <?php foreach ($cards as $card) : ?>
        <div class="card is-dashed">
            <?= $card->entry()->kt() ?>
        </div>
        <?php endforeach ?>
    </div>
</section>
<?php endslot() ?>

<?php snippet('footer') ?>
