<section class="container">
    <div class="mt-12 px-8 py-6 card is-dashed">
        <?php snippet('layouts/styles/default', compact('layout')) ?>
    </div>
</section>
