<?php snippet('header') ?>

<?php if ($header = $slots->header()) : ?>
<?= $header ?>
<?php else : ?>
<header class="container">
    <div class="flex flex-col lg:flex-row">
        <div class="flex-1">
            <?= $page->text()->kt() ?>
        </div>
        <div class="mt-12 flex-none text-center">
            <?php snippet('cover') ?>
        </div>
    </div>
</header>
<?php endif ?>

<?php if ($body = $slots->body()) echo $body; ?>

<?php if ($footer = $slots->footer()) echo $footer; ?>

<?php snippet('footer') ?>
