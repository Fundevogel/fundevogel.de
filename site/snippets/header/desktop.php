<nav class="spread-out flex-1 text-white text-shadow hidden lg:block">
    <ul class="flex items-center justify-between text-sm" role="menu" itemscope itemtype="https://schema.org/SiteNavigationElement">
        <?php foreach($pages->listed()->onlyTranslated() as $item) : ?>
        <li class="px-2" role="menuitem">
            <a
                class="js-tippy flex items-center text-white relative outline-none<?php e($item->isOpen(), ' is-active') ?>"
                href="<?= $item->url() ?>"
                title="<?php e($item->isHomePage(), t('menu-home'), $item->title()->html()) ?>"
                data-template="js-<?= $item->intendedTemplate() ?>"
                itemprop="url"
            >
                <span itemprop="name"><?= t('menu-' . $item->id()) ?></span>
            </a>
        </li>
        <?php endforeach ?>
    </ul>
</nav>
<div class="spread-out lg:ml-6 lg:pl-8 flex-none flex justify-between text-sm lg:border-l-2 border-red-light">
    <?php if ($site->shop()->isNotEmpty()) : ?>
    <a
        class="js-tippy mr-4 hidden lg:flex items-center text-white relative outline-none"
        href="<?= $site->shop() ?>"
        title="<?= t('menu-shop') ?>"
        data-template="js-undefined"
        target="_blank"
        rel="noopener"
    >
        <?= useSVG(t('menu-shop'), 'w-6 h-6 fill-current', 'cart') ?>
        <span class="px-2">Shop</span>
    </a>
    <?php endif ?>

    <!-- LANGUAGE MENU -->
    <?php if ($page->hasTranslations()) : ?>
    <div class="flex items-center z-50">
        <?= useSVG(t('Sprachauswahl'), 'w-6 h-6 fill-current', 'globe') ?>
        <nav class="ml-2 lg:ml-0 flex items-center">
            <?php
                foreach ($kirby->languages() as $language) :
                if ($page->isTranslated($language->code())) :
                $from = Kirby\Toolkit\Str::upper($kirby->language());
                $to = Kirby\Toolkit\Str::upper($language->code());
            ?>
            <a
                class="js-tippy<?php e($kirby->language() == $language, ' hidden is-active ', ' flex justify-center ') ?>px-2 text-white outline-none <?= $language->code() ?>"
                href="<?= $page->url($language->code()) ?>"
                title="<?= t(sprintf('%s nach %s', $from, $to)) ?>"
            >
                <span><?= $language->code() ?></span>
            </a>
            <?php
                endif;
                endforeach;
            ?>
        </nav>
    </div>
    <?php endif ?>
</div>
