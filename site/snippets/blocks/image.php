<?php if ($image = $block->image()->toFile()) : ?>
<figure class="my-4 px-4 text-center">
    <?= $image->toTag('inline-block mb-4', 'cover') ?>
    <?php if ($block->caption()->isNotEmpty()): ?>
    <figcaption class="flex justify-center font-medium text-xs">
        <span class="max-w-xl text-center">
            <?= $block->caption() ?>
        </span>
    </figcaption>
    <?php endif ?>
</figure>
<?php endif ?>
