<?php

$data = [
    'text' => $block->text(),
    'author' => $block->citation(),
    'color' => $block->color(),
];

snippet('base/quote', $data);
