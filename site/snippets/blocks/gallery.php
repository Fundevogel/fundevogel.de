<?php

if ($block->images()->isEmpty()) {
    return false;
}

$data = [
    'heading' => $block->heading(),
    'icon' => $block->icon(),
    'images' => $block->images()->toFiles(),
];

snippet('base/gallery', $data);
