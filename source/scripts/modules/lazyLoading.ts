import lazySizes from 'lazysizes';

import 'lazysizes/plugins/native-loading/ls.native-loading';
import 'lazysizes/plugins/progressive/ls.progressive';
import 'lazysizes/plugins/respimg/ls.respimg';


lazySizes.cfg.init = false;
lazySizes.cfg.loadMode = 1;

export default (): void => lazySizes.init();
