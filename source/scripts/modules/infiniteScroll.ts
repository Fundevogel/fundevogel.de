import InfiniteScroll from 'infinite-scroll';

export default (container: HTMLElement): InfiniteScroll => {
    return new InfiniteScroll(container.querySelector('.js-list'), {
        hideNav: '.js-hide',
        button: '.js-more',
        path: '.js-target',
        append: '.js-article',
        scrollThreshold: false,
        history: false,
    });
};
