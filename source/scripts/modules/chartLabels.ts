import forEach from '../helpers/forEach';

export default (container: HTMLElement): void => {
    forEach(container.querySelectorAll('.js-label'), (value: HTMLElement) => {
        value.style.background = value.dataset.color;
    });
};
