export default (number: number): Promise<void> => {
    number = number || 2000;

    return new Promise<void> (done => {
        setTimeout(() => {
            done();
        }, number)
    });
};
