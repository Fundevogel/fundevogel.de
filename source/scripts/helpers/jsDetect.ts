export default () => {
    const html = document.documentElement;
    let className: string;

    className = html.className.replace('no-js', 'js');
    html.className = className;
}
