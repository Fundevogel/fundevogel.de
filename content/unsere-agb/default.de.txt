Title: Unsere AGB

----

Layouts: [{"attrs":{"style":"default","heading":"","icon":""},"columns":[{"blocks":[{"content":{"level":"h3","text":"\u00a7 1 Geltungsbereich"},"id":"de148e7e-5193-4639-8fa8-97edd7151815","isHidden":false,"type":"heading"},{"content":{"text":"<p>F\u00fcr die Gesch\u00e4ftsbeziehung zwischen der Buchhandlung Fundevogel (im Folgenden: Verk\u00e4ufer) und dem Besteller gelten die nachfolgenden Allgemeinen Gesch\u00e4ftsbedingungen.<\/p>"},"id":"33e71d5a-011f-4df5-a27c-41ac8e9f438b","isHidden":false,"type":"text"},{"content":[],"id":"199747fe-9c8f-464b-96a7-d48734283fdd","isHidden":false,"type":"line"},{"content":{"level":"h3","text":"\u00a7 2 Bestellung"},"id":"f4e1dd63-1e01-418a-a594-0b15e0575987","isHidden":false,"type":"heading"},{"content":{"text":"<p>Ihr habt die M\u00f6glichkeit, unser Angebot in aller Ruhe zu durchst\u00f6bern. Wenn Ihr etwas Interessantes entdeckt habt, legt Ihr das Produkt in Euren virtuellen Warenkorb. Dazu klickt Ihr auf das kleine Warenkorbsymbol, das wir neben dem Produkt eingeblendet haben. Selbstverst\u00e4ndlich habt Ihr die M\u00f6glichkeit, Produkte jederzeit wieder aus Ihrem Warenkorb herauszunehmen.<\/p><p>Den Bestellvorgang leitet Ihr ein, indem Ihr das Schaltfeld \u201eZur Kasse\u201c anklickt. Ihr werdet hierauf gebeten, Euch mit Euren Nutzerdaten anzumelden bzw. als Neukund:in ein Nutzer:innenkonto einzurichten. Ihr k\u00f6nnt Euch aber auch als Gast anmelden. Dabei werden Eure Daten nur tempor\u00e4r erfasst. Im Anschluss k\u00f6nnt Ihr wahlweise Eure Adresse angeben, um die bestellten Titel per Post zu erhalten oder Ihr w\u00e4hlt unsere Buchhandlung als Lieferort aus. \u00dcber den Button \u201eWeiter\u201c gelangt Ihr zur Bestell\u00fcbersicht. Hier erhaltet Ihr eine komplette \u00dcbersicht \u00fcber die bestellten Titel und die gesamten Kosten. Ihr habt hier ebenfalls die M\u00f6glichkeit, Eure Bestellung zu ver\u00e4ndern oder abzubrechen.<\/p><p>Durch Bet\u00e4tigen der Schaltfl\u00e4che \u201eKauf abschlie\u00dfen\u201c wird Eure Bestellung ausgel\u00f6st und an uns weitergeleitet. Mit Eurer Bestellung erkl\u00e4rt Ihr verbindlich, die Ware erwerben zu wollen und akzeptiert unsere Allgemeinen Gesch\u00e4ftsbedingungen.<\/p>"},"id":"fe512605-ee47-48e2-b349-7bedee086abf","isHidden":false,"type":"text"},{"content":[],"id":"3b20269e-2c82-470e-8486-f48fed242826","isHidden":false,"type":"line"},{"content":{"level":"h3","text":"\u00a7 3 Vertragsschluss"},"id":"c391a7aa-cb02-46af-b580-ab563a3c16c2","isHidden":false,"type":"heading"},{"content":{"text":"<p>Bei einer Bestellung schlie\u00dft Ihr einen Vertrag mit:<\/p><p>Buchhandlung Fundevogel, vertreten durch dessen Inhaber Claus-Peter Jepsen, Marienstra\u00dfe 13, 79098 Freiburg im Breisgau.<\/p><p>Ihr werdet \u00fcber den Eingang Eurer Bestellung umgehend per E-Mail informiert. Die Auftragsbest\u00e4tigung erfolgt automatisch und stellt noch keine Vertragsannahme dar. Der Vertrag \u00fcber den Erwerb eines Produktes kommt dadurch zustande, dass wir Eure Bestellung durch Lieferung oder Bereitstellung der Ware im Ladengesch\u00e4ft oder durch eine Best\u00e4tigung per Email annehmen.<\/p><p>Die f\u00fcr den Vertragsschluss zur Verf\u00fcgung stehende Sprache ist Deutsch.<\/p>"},"id":"e8ca21ab-0bfc-4647-9cc6-11ef63465634","isHidden":false,"type":"text"},{"content":[],"id":"ae18348c-5948-427c-bc04-c2be6ada9102","isHidden":false,"type":"line"},{"content":{"level":"h3","text":"\u00a7 4 Vorbehalt der Nichtverf\u00fcgbarkeit"},"id":"cc9580fa-821b-4731-a10d-7771a8f10570","isHidden":false,"type":"heading"},{"content":{"text":"<p>Wir behalten uns vor, von einer Ausf\u00fchrung Eurer Bestellung abzusehen, wenn wir den bestellten Titel nicht vorr\u00e4tig haben, der nicht vorr\u00e4tige Titel beim Verlag vergriffen und die bestellte Ware infolgedessen nicht verf\u00fcgbar ist. In diesem Fall werden wir Euch unverz\u00fcglich \u00fcber die Nichtverf\u00fcgbarkeit informieren und einen gegebenenfalls von Euch bereits gezahlten Kaufpreis unverz\u00fcglich r\u00fcckerstatten.<\/p>"},"id":"7f904e73-156d-43cc-b885-632d8be77d47","isHidden":false,"type":"text"},{"content":[],"id":"cc342a3a-8f49-4f45-9857-e9ffdcdde9ee","isHidden":false,"type":"line"},{"content":{"level":"h3","text":"\u00a7 5 Versandkosten"},"id":"f37ba6be-5040-48b9-af4c-257d59c26093","isHidden":false,"type":"heading"},{"content":{"text":"<p><strong>Gratis-Lieferung in die Buchhandlung:<\/strong> Die Ware kann bei uns im Ladengesch\u00e4ft abgeholt werden und ist in diesem Fall selbstverst\u00e4ndlich versandkostenfrei. Die Lieferung sofort lieferbarer Titel k\u00f6nnen in der Regel am n\u00e4chsten Werktag in der Buchhandlung abgeholt werden.<\/p><p>Solltet Ihr <strong>Versand<\/strong> w\u00fcnschen, wird die Ware nach Eingang in unserer Buchhandlung sofort mit Rechnung an Euch versandt. In der Regel liefern wir auf dem Postweg, f\u00fcr dessen Laufzeit wir keine Verantwortung \u00fcbernehmen k\u00f6nnen. Die Lieferung erfolgt innerhalb Deutschlands erfahrungsgem\u00e4\u00df innerhalb von 1-3 Werktagen.<\/p>"},"id":"83c2a434-ac34-44bd-9b8c-5cff690c7a6f","isHidden":false,"type":"text"},{"content":{"text":"<ul><li>Innerhalb Deutschlands liefern wir ab einem Bestellwert von EUR 50,- portofrei.<\/li><li>Bei einem Bestellwert unter 50,- Euro berechnen wir eine Versandkostenpauschale von EUR 2,50.<\/li><li>Bei Lieferungen ins europ\u00e4ische Ausland erheben wir eine Versandkostenpauschale von EUR 7,50.<\/li><li>Bei Lieferungen ins nicht-europ\u00e4ische Ausland erheben wir eine Versandkostenpauschale von EUR 25,00.<\/li><\/ul>"},"id":"a307130f-abd2-48d2-94c9-800d4afd314b","isHidden":false,"type":"list"},{"content":[],"id":"726ef985-32c4-458c-9222-0f6ed571062c","isHidden":false,"type":"line"},{"content":{"level":"h3","text":"\u00a7 6 Zahlung"},"id":"ff53101f-4f2b-43cb-a24b-d779fd7c6741","isHidden":false,"type":"heading"},{"content":{"text":"<p>Der Kaufpreis ist sofort f\u00e4llig und ohne Abzug zahlbar, sp\u00e4testens innerhalb von 14 Tagen nach Rechnungsstellung auf folgende Kontoverbindung:<\/p><p>Buchhandlung Fundevogel, Claus-Peter Jepsen<br>Volksbank Freiburg<br>Konto 7240708<br>BLZ 680\u00a0900\u00a000<br>IBAN: DE 26\u00a06809\u00a00000\u00a00007\u00a02407\u00a008<br>BIC: GENODE61FR1<\/p><p>Bei Zahlung auf Rechnung verpflichtet Ihr Euch, den Rechnungsbetrag innerhalb von 14 Tagen nach Erhalt der Ware zu begleichen. Ihr k\u00f6nnt bar, mittels Kreditkarte, durch \u00dcberweisung oder im Lastschriftverfahren zahlen.<\/p><p>Solltet Ihr mit Euren Zahlungen in Verzug geraten, so behalten wir uns vor, Euch Mahngeb\u00fchren in Rechnung zu stellen. Die Kosten f\u00fcr eine evtl. R\u00fccklastschrift bei Lastschrift tr\u00e4gt immer der K\u00e4ufer. Bitte habt Verst\u00e4ndnis daf\u00fcr, dass wir uns im Einzelfall das Recht vorbehalten, nur gegen Vorauskasse zu liefern.<\/p><p><strong>R\u00fccksendekosten<\/strong><br>Im Falle eines Widerrufs (siehe Widerrufsrecht) habt Ihr die regelm\u00e4\u00dfigen Kosten der R\u00fccksendung zu tragen, wenn die gelieferte Ware der bestellten entspricht.<\/p>"},"id":"dc5dce64-2cae-46ab-968a-3e007532adea","isHidden":false,"type":"text"},{"content":[],"id":"85654074-041d-467c-ba17-0f0689a32145","isHidden":false,"type":"line"},{"content":{"level":"h3","text":"\u00a7 7 Eigentumsvorbehalt"},"id":"269b3704-f9cf-4013-a3fa-887afbe34947","isHidden":false,"type":"heading"},{"content":{"text":"<p>Bis zur vollst\u00e4ndigen Erf\u00fcllung der Kaufpreisforderung durch den Besteller verbleibt die gelieferte Ware im Eigentum des Verk\u00e4ufers.<\/p>"},"id":"628f7e87-27d1-4da5-b029-bf2461f21fc5","isHidden":false,"type":"text"},{"content":{"level":"h3","text":"\u00a7 8 Gew\u00e4hrleistung"},"id":"7b851511-c361-4274-a41d-eb7eb49193d5","isHidden":false,"type":"heading"},{"content":{"text":"<p>Es gilt das gesetzliche M\u00e4ngelhaftungsrecht.<\/p>"},"id":"f6e1778b-4c00-4a65-abc9-23d365389caa","isHidden":false,"type":"text"},{"content":[],"id":"e8d9dbc6-48d3-45fc-9729-b6380c2b8098","isHidden":false,"type":"line"},{"content":{"level":"h3","text":"\u00a7 9 Anwendbares Recht und Gerichtsstand"},"id":"5815ae54-e5d5-430d-8e18-c7a8d0e8a0af","isHidden":false,"type":"heading"},{"content":{"text":"<p>Es gilt das Recht der Bundesrepublik Deutschland unter Ausschluss des UN-Kaufrechts.<\/p><p>Ist der Besteller Kaufmann oder juristische Person des \u00f6ffentlichen Rechts, ist ausschlie\u00dflicher Gerichtsstand f\u00fcr alle Streitigkeiten aus dem Vertragsverh\u00e4ltnis das f\u00fcr unseren Firmensitz Freiburg i.Brsg. zust\u00e4ndige Gericht.<\/p><p><strong>Streitbeilegung<\/strong><br>Die Europ\u00e4ische Kommission stellt eine <a href=\"http:\/\/ec.europa.eu\/consumers\/odr\" rel=\"noopener noreferrer nofollow\">Plattform zur Online-Streitbeilegung<\/a> (OS-Plattform) bereit. Verbraucher haben die M\u00f6glichkeit, diese Plattform f\u00fcr die Beilegung ihrer Streitigkeiten zu nutzen.<\/p>"},"id":"4b371d35-bc31-404a-80b5-584ed97582bd","isHidden":false,"type":"text"}],"id":"6fdf9dd3-ceb9-445f-bae8-de9a416f6d1e","width":"1\/1"}],"id":"0a164794-d6b9-4bf3-86bb-e7a2b33c8fe8"}]

----

Cover: 

----

Meta-title: 

----

Meta-description: Hier geht's zu den Allgemeinen Geschäftsbedingungen (kurz AGB) der Freiburger Kinder- und Jugendbuchhandlung Fundevogel.

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image: 

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 

----

Og-type-article-modified-time: 

----

Og-type-article-expiration-time: 

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image: 

----

Twitter-card-type: 

----

Twitter-site: 

----

Twitter-creator: 

----

Date: 2020-07-07

----

Meta-keywords: 