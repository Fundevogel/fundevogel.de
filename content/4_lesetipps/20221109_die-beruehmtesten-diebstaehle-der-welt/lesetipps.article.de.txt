Title: Die berühmtesten Diebstähle der Welt

----

Text:

Das Buch beinhaltet neun spektakuläre Fälle der Kriminalgeschichte der letzten 150 Jahre. Es wird anschaulich dargestellt, wie es zu den Diebstählen kam, wer sie geplant hat, wie sie durchgeführt wurden und wie die Arbeit der Polizei ablief, die schließlich zur Überführung der Täter geführt hat.

Die Leserinnen und Leser blättern in diesem Buch wie in alten Zeitungsberichten und werden atmosphärisch Stück für Stück durch den jeweiligen Fall geführt. Es ist wunderschön gestaltet und bietet somit einen gewissen Retro-Charme, der zum Inhalt des Buches perfekt passt. Fettgedruckte reißerische Überschriften und spannende Informationen unterstreichen diesen Eindruck eines Zeitungsberichtes. Die Texte werden durch Zeichnungen des Tatherganges, Lageplänen sowie Polizeibildern der Täter unterstützt. Sogar Fingerabdrücke wurden nachgezeichnet.

Soledad Romero Mariño und Julio Antonio Blasco beschreiben detailliert die neun Fälle, die sich zu verschiedenen Zeiten und in verschiedenen Ländern abgespielt haben. Natürlich darf dabei auch nicht der Raub der Mona Lisa fehlen, ebenso wie der große Postraubzug von 1963 oder der Einbruch in das Antwerpener Diamantenzentrum von 2003.

----

Verdict: Ob Kunstraub, Bankeinbrüche oder Hackerangriffe - raffiniert ausgeklügelte Raubzüge faszinieren viele Menschen. Neun spektakuläre Fälle der Kriminalgeschichte sind Gegenstand des Sachbuches "Die berühmtesten Diebstähle der Welt". Im Retro-Zeitungsstil wird äußerst unterhaltsam und anschaulich dargestellt, wie die Diebe vorgegangen sind und wie die Polizei den Tätern auf die Spur kam.

----

Conclusion:

Nicht nur die Höhe der jeweiligen Beute ist  unterschiedlich, sondern auch der Ausgang. Mal wurden die Täter gefasst, ein anderes Mal sind sie entkommen, manchmal wurden sie, wie bei dem geheimnisvollen Flugzeugentführer einer Boeing 727, nie identifiziert. Oder sie standen sogar vor Gericht und sind mitten im Prozess entkommen. Einige von ihnen sind sogar zu wahren Volkshelden gegen "das System" mutiert, wie El Dioni, der Bankräuber von Nizza.

Dieses Buch bietet zu jedem Diebstahl sehr viele Hintergrundinformationen. Spannend und unterhaltsam werden die spektakulärsten Diebstähle vorgestellt und das Ausmaß der jeweiligen Tat wird dadurch mehr als deutlich.

Über den Inhalt des Buchs staunen Kinder und Erwachsene gleichermaßen. Im Retro-Zeitungsstil begeistert dieses Buch nicht nur für die Geschichten selbst, sondern auch für die Ermittlungen und Reportagen.

----

Layouts: 

----

Date: 2022-11-09

----

Book:

- >
  buecher/die-beruehmtesten-diebstaehle-der-welt-hardcover

----

Isexternal: true

----

Isadvanced: false

----

Meta-title: 

----

Meta-description: 

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image: 

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 2022-11-09 16:30:00

----

Og-type-article-modified-time: 2022-11-09 16:30:00

----

Og-type-article-expiration-time: 2022-11-09 16:30:00

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image: 

----

Twitter-card-type: 

----

Twitter-site: 

----

Twitter-creator: 