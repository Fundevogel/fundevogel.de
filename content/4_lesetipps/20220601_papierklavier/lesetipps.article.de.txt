Title: Papierklavier

----

Text:

Der multimodale Text – bestehend aus Schrift und Zeichnungen – erweckt viel eher den Eindruck, ein authentisches Tagebuch zu sein, als dies bei thematisch vergleichbaren Werken der Fall ist. Dies liegt auch an der Schriftart, die eher an Comics als an monomodale Schriftmedien erinnert. Die kindliche Druckschrift ist manchmal von Kursivierungen und Durchstreichungen unterbrochen oder von herausgehobenen, im Graffitistil dargestellten Wörtern. Neben Skizzen findet man auch Listen, Dialoge, stichwortartige Gedanken, Träume, Erinnerungen, Ausschnitte aus Chats und ein Diagramm – eine Stresslevel-Kurve.

Bei „Papierklavier“ müssen Leserinnen und Leser Schrift und Bild zueinander in Beziehung setzen. Die Bilder sind also mehr als nur Illustrationen. Sie fügen dem Text neue Bedeutungsebenen hinzu. Beispielsweise sieht man neben Maias Beschreibung einer Beerdigung viele aufgesperrte Augen vor schwarzen Hintergrund, die alle einen Punkt zu fokussieren scheinen. Maias Großmutter ist gestorben. Neben der Trauer werden so weitere Emotionen ausgedrückt, je nach Betrachterin oder Betrachter, z.B. Angst, Unsicherheit, Unterlegenheit oder auch das Gefühl, nicht willkommen zu sein. Das letztgenannte Gefühl wird an verschiedenen Stellen thematisiert.

Die 15-jährige Maia wächst in schwierigen Verhältnissen auf. Dies wird erstmals deutlich, als sie sich von drei Euro Essensgeld am liebsten einen Cappuccino und einen Joghurt kaufen würde, dann aber für die ganze Familie Nudeln, Tomatensoße und Äpfel besorgt. In Maias Familie ist "alles zu knapp" – Geld, Platz und Zeit. Sie muss in einem Smoothie-Laden mehrere Schichten übernehmen, um für die Familie etwas dazuzuverdienen. Sie würde gerne noch mehr arbeiten, um ihrer Schwester Heidi Klavierstunden finanzieren zu können.  Früher spielte Heidi auf einem Klavier, das bei ihrer Großmutter stand. Nun muss sie mit auf Papier aufgezeichneten Tasten vorlieb nehmen, zu denen sie die Töne summt.

----

Verdict: Maia muss viel auf ihre kleine Schwester aufpassen, während ihre Mutter einem schlecht bezahlten Job nachgeht. Die 15-Jährige pfeift auf ihr Übergewicht und trotzt gemeinsam mit ihrer transgender Freundin den Sorgen des Alltags. Selbstbewusst und humorvoll berichtet sie uns davon in einem mit authentisch wirkenden Skizzen illustrierten Tagebuch.

----

Conclusion:

### Ihrem Umfeld voraus
Trotz aller Widerstände ist Maia ein fröhlicher Mensch, der "Alltagsglück"+ in kleinen Portionen sammelt und sich daran erfreut. Sie ist gedankenschnell. Im Unterricht kommt sie dennoch nicht gut mit, weil der Job im Smoothie-Laden ihr die Energie raubt. Sie will in der Klasse nicht zu sehr auffallen und ist nicht besonders beliebt, was sie auf ihr vermeintliches Übergewicht - Größe 42 - zurückführt. So schreibt sie in ihr Tagebuch: „Die beschissenste Gleichung der Welt: schlank = SCHÖN = wertvoll“. Derartig schonungslos-authentische Sprache verpackt viel Inhalt in wenigen Zeichen. Als sich ihre kleine Schwester im gemeinsamen Kinderzimmer an sie kuschelt, sagt Maia: "Es muss schön sein, sieben zu sein."

Mit dem Buch ist eine interessante öffentliche Kontroverse verbunden, die sich gut im Literaturunterricht integrieren lässt. Die Jury des Katholischen Jugend- und Kinderbuchpreises wollte „Papierklavier“ auszeichnen. Dies wurde durch die Deutsche Bischofskonferenz unterbunden. Die genauen Gründe sind nicht bekannt. Vielleicht liegt es daran, dass Maias Mutter drei Töchter von drei Vätern hat. Brisant könnte auch sein, dass Maias beste Freundin Carla als Kind Engelbert hieß und nun als „Frau mit Penis“ durchs Leben geht.

Auch zum Gendern hat Maia eine klare Position. Als eine Lehrerin nicht gendern möchte, bilanziert Maia: "Da wollen die immer, dass wir unser Hirn benutzen, und schalten ihr eigenes auch nicht ein." Weil Maia so authentisch, meinungs- und charakterstark ist, nimmt man ihr auch ab, wenn sie kritisch über Soziale Netzwerke und Influencer-Kultur spricht. Auch dann hat das Buch nicht den Anschein von pädagogischer Bevormundung ex cathedra. Maia ist ihrem Umfeld weit voraus, wenn sie beschließt, sich "in der eigenen Haut" - "im eigenen Leben" - wohlzufühlen. Das Buch stiftet Leserinnen und Leser dazu an, es ihr gleich zu tun.

----

Layouts: 

----

Date: 2022-06-01

----

Book:

- buecher/papierklavier-hardcover

----

Isexternal: true

----

Isadvanced: false

----

Meta-title: 

----

Meta-description: 

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image: 

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 2022-06-09 13:05:00

----

Og-type-article-modified-time: 2022-06-09 13:05:00

----

Og-type-article-expiration-time: 2022-06-09 13:05:00

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image: 

----

Twitter-card-type: 

----

Twitter-site: 

----

Twitter-creator: 