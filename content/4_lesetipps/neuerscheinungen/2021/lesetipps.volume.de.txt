Title: 2021

----

Text:

Hier findet Ihr unsere Empfehlungen für 2021 - neben viel Lesespaß und empfehlenswerten Hörbüchern für alle Altersstufen natürlich mit Oster-Spezial im Frühjahr und passenden Büchern zum Thema Weihnachten in der Herbstausgabe.

Bei den Empfehlungen für das Frühjahr sind außerdem zum zweiten Mal überhaupt auch Graphic Novels dabei!