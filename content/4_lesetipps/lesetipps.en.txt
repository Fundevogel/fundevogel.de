Title: Reading Tips

----

Slug: recommendations

----

Text:

## Our recommendations
We just love reading new and exciting books, but we don't stop there - whenever we discover something truly inspiring and fascinating, we want to share it with you.

Aside from that, every year we put together two lists of selected novelties, forming our spring and autumn recommendation editions.

Be sure to download the latest issues from the links to the right or  subscribe to our (email: info@fundevogel.de text: mailing list). For earlier editions, have a look at the (link: lesetipps/neuerscheinungen text: archive).

----

Meta-title: 

----

Meta-description: We just love reading new and exciting books - and whenever we discover something truly inspiring and fascinating, we want to share it with you.

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image: 

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 

----

Og-type-article-modified-time: 

----

Og-type-article-expiration-time: 

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image: 

----

Twitter-card-type: summary_large_image

----

Twitter-site: 

----

Twitter-creator: 

----

Meta-keywords: 