Title: Zin

----

Text:

Die titelgebende Figur wäre eigentlich im schulfähigen Alter. Doch weil Zineddines Familie auch auf seine finanzielle Unterstützung angewiesen ist, fängt der Sohn an, in einer Druckerei zu arbeiten. Begleitend zu seiner Arbeit lernt er Buchstabe für Buchstabe das arabische Alphabet kennen. Dabei beginnt seine Entdeckungsreise bei seinem eigenen Namen. Gleich am ersten Tag zeigt der Meister der Werkstatt seinem neuen Schützling das Druckverfahren beispielhaft an der Kose- und Kurzform seines Namens: Zin (زين). "Wunderschön" zeigt sich der Schriftzug, als der Meister den bedruckten Papierbogen aus der Druckpresse zieht.

### Von der Macht der Buchstaben
Zins erster Arbeitstag ist für ihn somit Initialmoment und Ausgangspunkt, die Welt der Schrift zu entdecken und sich ihrer zu ermächtigen. Über die Zusammensetzung der beweglichen Lettern zu Wörtern, die dann in einem Bleisatz als Textblock miteinander verbunden werden, wird Zin vertraut mit den Bausteinen der arabischen Schrift. Aus diesen bildet er für ihn bedeutsame Begriffe ab und zerlegt Wörter, die ihm bei seinen Wegen durch die Stadt begegnen, in Einzelteile.

Ladenschilder, Zeitungen und Bücher sind seine Quellen. Was er nicht lesen kann, schreibt er ab, und lässt sich später von seinem Meister dabei helfen, die noch unbekannten Wörter zu verstehen. Mit Zins Fähigkeiten wachsen seine Aufgaben in der Werkstatt und sein Horizont. Als er eines Tages auf dem menschenleeren Weg nach Hause von einem Polizisten angehalten wird, realisiert nicht nur er, sondern auch die Leser:in, dass sich etwas verändert hat. Während auf der einen Seite Menschen fröhlich ins Gespräch vertieft das Straßenbild kennzeichnen, dominiert das Porträt des Polizisten, dessen Augen von einer Schirmmütze verdeckt sind, mit starken Konturen eine gesamte Bilderbuchseite.

Die Bedrohung zeigt sich im Bildaufbau, in Bildausschnitten, Perspektiven und Kontrasten. Der Spannungsbogen wird beim Umblättern jedoch wieder aufgelöst, als ein Mann mit Flugblättern um sich wirft. Zin liest mit klopfendem Herz vom Streikaufruf und erkennt: "Dieses Blatt kannte er. Er hatte es zusammen mit dem Meister gedruckt, die Farbe klebte noch an seinen Fingern."

----

Verdict: Hassan Zahreddine erzählt die Geschichte eines Neunjährigen, der aufgrund der ärmlichen Umstände gezwungen ist, Geld zu verdienen. Über die Arbeit in der Druckerei bringt er sich selbst das Lesen bei und lernt bei der Gelegenheit auch die Macht der Wörter kennen. Damit erzählt das Bilderbuch eine Erfolgsgeschichte, die das Thema Emanzipation und Empowerment ins Zentrum setzt.

----

Conclusion:

### Von der Schwarzkunst
Analog zur Erzählebene hält Hassan Zahreddine ausgewählte Aspekte der Geschichte gleich Momentaufnahmen mit der druckgrafischen Technik Mezzotinto fest. Dieses sehr aufwändige und deswegen heute nur noch selten verwendete grafische Verfahren verleiht der Erzählung eine dokumentarische Wirkung. Bei der Mezzotinto-Technik, auch Schwarzkunst genannt, werden Kupferplatten mit speziellen Wiegemessern, Schabeisen und Polierstahl bearbeitet. Je nachdem wie glatt oder rau die Plattenoberfläche bearbeitet wird, entscheidet darüber, ob mehr oder weniger Druckschwärze aufgenommen wird.

Die Möglichkeit mit der Drucktechnik zwischen sehr hellen und dunklen Tonwerten Licht und Schatten erzeugen zu können, nutzt der libanesische Druckkünstler, um die Geschichte stimmungsvoll in Einzelbildern zu ergänzen. Über den Kontrast von mattschwarzen Flächen und Stellen, in denen die Rillen des Wiegemessers noch sichtbar sind, entsteht zudem eine besondere Tiefenwirkung.

Mit dieser öffnet Zahreddine den Raum des Bilderbuchs und lädt die Betrachter:innen zu einem genauen Erkunden der Einzelbilder ein. Die noch zu erkennenden Ränder der Kupferplatten rahmen die Einzelbilder als Unikate, die Fotografien ähnlich Zeugnis geben. Durch Skizzen ergänzt, fügen sich die Drucke mit den Erzähltextblöcken in ein grafisches Gesamtbild und demonstrieren Buchkunst im Bilderbuchformat.

### Vom Einzelbuchstaben zur Revolution
Hassan Zahreddine gelingt es mit diesem erzählerischen Gesamtkunstwerk, die visuelle und narrative Ebene auf komplexe Art miteinander zu verknüpfen. In erzählerischer Genialität nutzt der Druckkünstler die unterschiedlichen Erzähldimensionen des Bilderbuchs, um die individuelle, soziale und kulturelle Relevanz des Drucks zu veranschaulichen. Zins Geschichte illustriert beispielhaft die vielfältigen Bedeutungsebenen des gedruckten Wortes. Das Bilderbuch schafft es somit, eine die Gesellschaft so stark verändernde Entwicklung literar-ästhetisch erfahrbar zu machen.

----

Layouts: 

----

Date: 2022-08-01

----

Book:

- buecher/zin-hardcover

----

Isexternal: false

----

Isadvanced: false

----

Meta-title: 

----

Meta-description: 

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image: 

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 2022-08-03 11:40:00

----

Og-type-article-modified-time: 2022-08-03 11:40:00

----

Og-type-article-expiration-time: 2022-08-03 11:40:00

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image: 

----

Twitter-card-type: 

----

Twitter-site: 

----

Twitter-creator: 