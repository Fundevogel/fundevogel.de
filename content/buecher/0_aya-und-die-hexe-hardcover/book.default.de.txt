Title: Aya und die Hexe

----

Isbn: 978-3-426-22770-1

----

Subtitle: 

----

Type: Hardcover

----

Author: Diana Wynne Jones

----

Illustrator: Miho Satake

----

Translator: Oliver Plaschka

----

Publisher: Knaur

----

Age: ab 10 Jahren

----

Antolin: 

----

Releaseyear: 2022

----

Binding: gebunden

----

Pagecount: 112

----

Price: 15,00

----

Topics: Hexe, Humor, Magie, Waisenhaus, Waisenkind, Zauberei

----

Description:

Fröhlich, frech, fantastisch: das humorvolle Fantasy-Märchen von Kult-Autorin Diana Wynne Jones als illustrierte SchmuckausgabeAuf gar keinen Fall will die 10-jährige Waise Aya adoptiert werden: Nirgendwo könnte es so schön sein wie im St.-Morwald-Waisenhaus, wo alle immer ganz genau das tun, was sie sich wünscht. Dummerweise taucht eines Tages ein seltsames Paar auf, das sich durch nichts davon abbringen lässt, Aya mitzunehmen. Eigentlich ist Ayas neues Zuhause gar nicht so schlecht: Es steckt voller unsichtbarer Räume und geheimnisvoller Zauberbücher, und einen sprechenden Kater gibt es auch. Kein Wunder: Aya ist im Haus der mächtigen Hexe Bella Yaga gelandet! Dass sie der Hexe als Assistentin zu Diensten sein soll, geht dann aber doch entschieden zu weit für ein Mädchen, das seinen eigenen Kopf hat ...

Das zauberhafte Fantasy-Märchen wurde von Studio Ghibli (u. a. "Chihiros Reise ins Zauberland") verfilmt. Die farbenfrohen Illustrationen der japanischen Künstlerin Miho Satake machen die Schmuckausgabe von "Aya und die Hexe" zu einem besonderen Geschenk für Fantasy-Fans.

----

Isavailable: true

----

Olacode: 

----

Olamessage: 

----

Shop: https://fundevogel.buchkatalog.de/webapp/wcs/stores/servlet/Product/4099276460880644535/63715/10002/-3/Humor-und-Comedy_weitere-Kategorien/Diana-Wynne-Jones/Aya-und-die-Hexe

----

Drawer: 

----

Photographer: 

----

Editor: 

----

Participants: 

----

Cover:

- aya-und-die-hexe_diana-wynne-jones.jpg

----

Categories: 

----

Isseries: false

----

Series: 

----

Volume: 

----

Hasaward: false

----

Award: 

----

Awardedition: 

----

Leselink: 

----

Participant: 