Title: Der Junge, der mit den Wölfen spricht

----

Isbn: 978-3-522-18589-9

----

Subtitle: 

----

Type: Hardcover

----

Author: Sam Thompson

----

Illustrator: 

----

Translator: Ingo Herzke

----

Publisher: Thienemann

----

Age: ab 10 Jahren

----

Antolin: 5. Klasse

----

Releaseyear: 2022

----

Binding: gebunden

----

Pagecount: 208

----

Price: 15,00

----

Topics: Abenteuer, Außenseiter, Befreiung, Fabelwesen, Freundschaft, Mobbing, Stille

----

Description: Silas wird in der Schule gemobbt, weil er nicht spricht. Eines Tages hilft er einem verletzten Wolf - und darf zum Dank eine verborgene Welt sprechender Tiere betreten, eine Welt, in der Sprache Macht bedeutet. Denn tief im Wald leben Füchse in einer unterirdischen Stadt. Sie manipulieren die Wölfe und unterdrücken sie. Silas möchte seinen Wolfsfreunden helfen, sich von den schlauen Füchsen zu befreien. Aber das geht nur, wenn es ihm gelingt, seine Stimme zu finden.

----

Isavailable: true

----

Olacode: 

----

Olamessage: 

----

Shop: https://fundevogel.buchkatalog.de/webapp/wcs/stores/servlet/Product/4099276460880670309/63715/10002/-3/Buecher_Kinder--und-Jugend/Sam-Thompson/Der-Junge-der-mit-den-Woelfen-spricht

----

Drawer: 

----

Photographer: 

----

Editor: 

----

Participants: 

----

Cover:

- >
  der-junge-der-mit-den-woelfen-spricht_sam-thompson.jpg

----

Categories: Kinderbuch

----

Isseries: false

----

Series: 

----

Volume: 

----

Hasaward: false

----

Award: 

----

Awardedition: 

----

Leselink: 

----

Participant: 