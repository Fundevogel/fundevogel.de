Title: Mehr. Mehr. Mehr.

----

Isbn: 978-3-906876-26-9

----

Subtitle: 

----

Type: 

----

Author: Franco Supino

----

Illustrator: 

----

Translator: 

----

Publisher: da bux, Werdenberg

----

Age: ab 12 Jahren

----

Antolin: 

----

Releaseyear: 2021

----

Binding: Taschenbuch

----

Pagecount: 52

----

Price: 13,24

----

Topics: Essstörung, Krankheit, Angst, Scham

----

Description: Da ist jemand, der deine Geschichte kennt. Und er stellt dir ein Ultimatum. Unsinn, denkst du. Niemand weiss, was du heimlich tust. Wer du wirklich bist. Welche Lüge du lebst.

----

Isavailable: false

----

Olacode: 

----

Olamessage: 

----

Shop: 

----

Drawer: 

----

Photographer: 

----

Editor: 

----

Participants: 

----

Cover:

- mehr-mehr-mehr_franco-supino.jpg

----

Categories: Jugendbuch

----

Isseries: false

----

Series: 

----

Volume: 

----

Hasaward: true

----

Award: LesePeter

----

Awardedition: Februar 2022

----

Leselink: https://www.gew.de/aktuelles/detailseite/schonungsloser-einblick-in-das-thema-essstoerungen