Title: Mission Kolomoro oder: Opa in der Plastiktüte

----

Isbn: 978-3-7512-0052-3

----

Subtitle: 

----

Type: Hardcover

----

Author: Julia Blesken

----

Illustrator: Barbara Jung

----

Translator: 

----

Publisher: Oetinger

----

Age: ab 9 Jahren

----

Antolin: 5. Klasse

----

Releaseyear: 2021

----

Binding: gebunden

----

Pagecount: 288

----

Price: 15,00

----

Topics: Abenteuer, Außenseiter, Freundschaft, Großstadt, Homosexualität, Magie, Tod, Alter, Familie, Diversität

----

Description: Vor einem Supermarkt, am Anfang der Herbstferien, treffen sechs Kinder zufällig aufeinander: Katja, die sich mit ihren Vätern gestritten hat. Polina, die nur eben Backpulver kaufen wollte, Fridi, Mustafa und Zeck sowie Jennifer mit Rehpinscher Püppi und der Asche ihres Opas in einer Plastiktüte. Als Mustafa einen Rocker auf dem Parkplatz reinlegt, müssen die Kinder schnellstens abhauen. Ohne Handys und fast ohne Geld. Aber mit einer wichtigen Mission: Jennifers Opa soll seine letzte Ruhe in Kolomoro finden. Nur: Wie geht das, wenn man keine Ahnung hat, wo Kolomoro liegt?

----

Isavailable: true

----

Olacode: 

----

Olamessage: 

----

Shop: https://fundevogel.buchkatalog.de/webapp/wcs/stores/servlet/Product/3000003547899/63715/10002/-3/Buecher_Kinder--und-Jugend/Julia-Blesken/Mission-Kolomoro-oder-Opa-in-der-Plastiktuete

----

Drawer: 

----

Photographer: 

----

Editor: 

----

Participants: 

----

Cover:

- >
  mission-kolomoro-oder-opa-in-der-plastiktuete_julia-blesken.jpg

----

Categories: Kinderbuch

----

Isseries: false

----

Series: 

----

Volume: 

----

Hasaward: true

----

Award: LesePeter

----

Awardedition: Januar 2022

----

Leselink: https://www.gew.de/aktuelles/detailseite/kinderbuch-mission-kolomoro-praemiert

----

Participant: 