Title: Absolut rekordverdächtig

----

Isbn: 978-3-522-30606-5

----

Subtitle: Dein Leben in Zahlen | Kindersachbuch ab 8 Jahren

----

Type: Hardcover

----

Author: Christoph Drösser

----

Illustrator: Nora Coenenberg

----

Translator: 

----

Publisher: Gabriel in der Thienemann-Esslinger Verlag GmbH

----

Age: ab 8 Jahren

----

Antolin: 4. Klasse

----

Releaseyear: 2022

----

Binding: gebunden

----

Pagecount: 112

----

Price: 14,00

----

Topics: Konsum, Lebensdauer, Lebensmittel, Nachhaltigkeit, Staunen, Verbrauch

----

Description: Ein Mensch in Deutschland lebt durchschnittlich 30.000 Tage. Wer hätte gedacht, dass wir fast 10.000 Tage davon verschlafen? Oder dass wir in der Zeit 400.000 Mal pupsen?Mal lustig, mal verblüffend, mal lehrreich - aber immer spannend: Das Erfolgs-Duo Christoph Drösser und Nora Coenenberg zeigt anschaulich, welche großen und kleinen Rekorde wir im Laufe eines Durchschnittslebens aufstellen. Spielerisch brechen sie komplexe Themen wie Nachhaltigkeit, Konsum und das menschliche Zusammenleben auf.Nach 100 Kinder (Gewinner des Deutschen Jugendliteraturpreis 2021) und Es geht um die Wurst das dritte Buch von Christoph Drösser und Nora Coenenberg.

----

Isavailable: true

----

Olacode: 

----

Olamessage: 

----

Shop: https://fundevogel.buchkatalog.de/webapp/wcs/stores/servlet/Product/4099276460880670345/63715/10002/-3/Buecher_Kinder--und-Jugend/Christoph-Droesser/Absolut-rekordverdaechtig

----

Drawer: 

----

Photographer: 

----

Editor: 

----

Participants: 

----

Cover:

- >
  absolut-rekordverdaechtig_christoph-droesser.jpg

----

Categories: Sachbuch, Kinderbuch

----

Isseries: false

----

Series: 

----

Volume: 

----

Hasaward: false

----

Award: 

----

Awardedition: 

----

Leselink: 

----

Participant: 