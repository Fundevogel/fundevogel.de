Title: Ein Sommer mit Percy und Buffalo Bill

----

Isbn: 978-3-8251-5283-3

----

Subtitle: 

----

Type: Hardcover

----

Author: Ulf Stark

----

Illustrator: Regina Kehn

----

Translator: Birgitta Kicherer

----

Publisher: Urachhaus

----

Age: ab 6 Jahren

----

Antolin: 

----

Releaseyear: 2021

----

Binding: gebunden

----

Pagecount: 285

----

Price: 18,00

----

Topics: Erste Liebe, Familie, Ferien, Freundschaft, Großeltern, Kindheit, Schweden, Sommer, Urlaub, Natur

----

Description:

Dieser Percy! Lädt sich einfach selber ein. Dabei mag Ulfs Großvater keinesfalls noch ein Gör in den Ferien zu Gast haben. Und prompt stellt Percy Ulfs Ferienleben auf den Kopf. Wenn auch nicht wie gedacht, die Freunde, Pia und der jähzornige Großvater finden den einfallsreichen Frechdachs nämlich toll. Und Percy liest Buffalo Bill und wagt das Unmögliche ... Ein Sommer, so würzig, so voller verrückter Einfälle, so dramatisch und großartig, wie ein Kindheitssommer nur sein kann!

Ulf Stark erzählt dieses Freundschafts-Sommerabenteuer in seinem unverwechselbaren Ton: schelmisch und liebevoll, mal urkomisch, mal todernst. Regina Kehn verleiht der Geschichte mit ihren frischen, witzigen und ausdrucksvollen Bildern dazu das Gesicht eines neuen Lieblingsbuchs.

----

Isavailable: true

----

Olacode: 

----

Olamessage: 

----

Shop: https://fundevogel.buchkatalog.de/webapp/wcs/stores/servlet/Product/3000003783166/63715/10002/-3/Buecher_Kinder--und-Jugend/Ulf-Stark/Ein-Sommer-mit-Percy-und-Buffalo-Bill

----

Drawer: 

----

Photographer: 

----

Editor: 

----

Participants: 

----

Cover:

- >
  ein-sommer-mit-percy-und-buffalo-bill_ulf-stark.jpg

----

Categories: Kinderbuch, Vorlesebuch

----

Isseries: false

----

Series: 

----

Volume: 

----

Hasaward: false

----

Award: 

----

Awardedition: 

----

Leselink: 

----

Participant: 