Title: Über Spanien lacht die Sonne

----

Isbn: 978-3-95640-212-8

----

Subtitle: 

----

Type: Hardcover

----

Author: Kathrin Klingner

----

Illustrator: 

----

Translator: 

----

Publisher: Reprodukt

----

Age: ab 14 Jahren

----

Antolin: 

----

Releaseyear: 2020

----

Binding: kartoniert/broschiert

----

Pagecount: 128

----

Price: 20,00

----

Topics: Digitale Welten, Digitales Zeitalter, Humor, Graphic Novel

----

Description: Kitty liest privat eher selten Internetkommentare. Bei der Arbeit dafür neuerdings umso öfter. 2015 kennt das Netz plötzlich nur noch ein Thema: "Flüchtlingskrise". Kitty wird als Aushilfe bei einem Hamburger Medienunternehmen eingestellt, das Onlinekommentare überprüft. Hinter Lamellenvorhängen und Zimmerpflanzen moderieren sie und ihre KollegInnen die Welle von Hass und Verschwörungstheorien, die in jenem Jahr das Internet flutet. Der Chef bemüht sich, das Team mit Pep Talks und Glühwein bei Laune zu halten. Nur von den beunruhigenden Anrufen, die er immer häufiger von einer anonymen Nummer erhält, erzählt er seinen Mitarbeitern nichts...Nach "Katze hasst Welt" legt Kathrin Klingner ein neues Werk vor, dass die Absurdität des digitalen Zeitalters mit viel Humor auf den Punkt bringt.

----

Isavailable: true

----

Olacode: 

----

Olamessage: 

----

Shop: https://fundevogel.buchkatalog.de/webapp/wcs/stores/servlet/Product/3000002605279/63715/10002/-3/weitere-Kategorien_Humor-und-Comedy/Kathrin-Klingner/ueber-Spanien-lacht-die-Sonne

----

Drawer: 

----

Photographer: 

----

Editor: 

----

Participants: 

----

Cover:

- >
  ueber-spanien-lacht-die-sonne_kathrin-klingner.jpg

----

Categories: Comic, Jugendbuch

----

Isseries: false

----

Series: 

----

Volume: 

----

Hasaward: false

----

Award: 

----

Awardedition: 

----

Leselink: 

----

Participant: 