Title: In alter Frische

----

Text:

Vom 12. bis zum 15. Februar wurde bei uns geräumt und geschleppt, gestrichen und gewischt, geschraubt und gehämmert - jetzt erstrahlt der Fundevogel in neuem altem Glanz, frisch gestrichen und doch so gemütlich wie eh und je!

**Wir sind wieder da!**

Seht selbst - klickt Euch durch die Galerie, oder kommt uns direkt persönlich besuchen!

----

Date: 2022-02-18

----

Subtitle-wanted: false

----

Subtitle: 