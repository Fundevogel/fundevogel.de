Title: Herzlich Willkommen, Aileen!

----

Text:

Wer in den letzten Wochen den Fundevogel besucht hat, konnte sie bereits kennenlernen - nun wollen wir unsere neue Mitstreiterin auch an dieser Stelle offiziell *Trommelwirbel* herzlich willkommen heißen! Aileen, magst Du Dich einfach mal vorstellen?

> Hallo ihr Lieben,
> ich bin Aileen Krieg, junge 31 Jahre alt und komme aus dem schönen Südbaden. Ich liebe es, fremde, magische Welten zu bereisen und kann mich als geübte Reisebegleiterin durch Raum und Zeit anbieten.
>
> Meine eigene Reise begann vor mehr als zehn Jahren mit einer Ausbildung zur Buchhändlerin, damals war mein Fachgebiet "Manga". Jedoch sollte es dabei nicht bleiben und ich sammelte Erfahrungen in der Science Fiction, Fantasy, Fantastik und im Comic-Genre. Egal, ob es für Erwachsene oder Jugendliche war, für ein gutes Buch war immer Zeit.
>
> Irgendwann verlagerten sich meine Interessen in die Grafik. Ich begann zu studieren und schloss meinen Bachelor in Grafikdesign mit dem Schwerpunkt "Illustrationen" erfolgreich ab. Danach zog es mich in die Schweiz. Schokogipfeli und Pendeln waren mehr als zwei Jahre meine tägliche Beschäftigung. Jeden Tag Grafiken erstellen und Werbetexte schreiben, ließ mich schnell feststellen, dieser Beruf füllte mich nicht aus.
>
> Schlussendlich bewarb ich mich im Fundevogel für die freie Stelle als Buchhändlerin. Ich wollte wieder zurück zu meinen Wurzeln, zurück zu den magischen Welten. Was ich gefunden habe, wisst ihr sicher, eine tolle schnuckelige, mit Leidenschaft geführte Buchhandlung für Kinder, Jugendliche und jung Gebliebene!
>
> Ich freue mich auf die Zeit mit dem Fundevogel-Team und euch als meine Reisegäste.

----

Date: 2021-12-22

----

Subtitle-wanted: true

----

Subtitle: Verstärkung für den Fundevogel