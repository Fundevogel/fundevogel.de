Title: Wir stehen zusammen

----

Text:

Das humanistische Erbe Europas ist dem russischen Machthaber Putin ein Dorn im Auge; umso wichtiger ist es, ein Signal der Solidarität auszusenden und dem menschenverachtenden Gewaltdenken dieses Diktators
die Verbundenheit und Entschlossenheit aller Völker Europas und der Welt entgegenzusetzen!

(quote: Wir sind nur so stark, wie wir vereint sind und so schwach, wie wir
gespalten sind. author: Albus Dumbledore)

Wir können viele konkrete Maßnahmen ergreifen, um aus der Ferne wirksam zu werden. Hier vor Ort hat sich die (link: https://blog.stadtmission-freiburg.de text: evangelische Stadtmission) für ein Kinderheim in der Ukraine eingesetzt. Wer mit anpacken will, kann das beispielsweise so tun:

- verlässliche Angebote für Kinder im Alter von 6-12 Jahren anbieten, vor Ort bei den Flüchtlings-Unterkünften: etwa eine Wanderung machen, Lagerfeuer, basteln (Material selbst organisieren!) - Kontakt: (email: aufrecht@dreisam3.de text: Norbert Aufrecht)
- Lebensmittel und Hygiene-Artikel, die mindestens fünf Monate haltbar sind (Müsli, Nudeln, Reis, Nüsse, Schokolade, Nutella, Toilettenpapier, Duschgel, Zahnpasta…) im Laufe der kommenden Woche einkaufen und zum (link: http://www.seinlaedele.de text: S’Einlädele) in der Guntramstraße bringen (aber Achtung: KEINE Kleidung!)
- wer einen Transporter zur Verfügung stellen kann, bitte bei (email: hoehlein@seinlaedele.de text: Volker Höhlein) melden -  es ist geplant, einen Transporter mit Lebensmitteln in die Ukraine zu schaffen, um Altenheime mit Essen etc. zu versorgen. Zwei ukrainische europäische Freiwillige würden diesen Transporter rüberfahren und versuchen, das Essen zu verteilen und Menschen wieder mit zurückzunehmen, die auf der Flucht sind

Weitere Hilfsmöglichkeiten findet Ihr auch bei der (link: https://www.freiburg.de/pb/1854105.html text: Stadt Freiburg).

(quote: Kannst du dir vorstellen, wie sehr Tyrannen die Menschen fürchten, die sie unterdrücken? Ihnen allen ist bewusst, dass ganz sicher eines Tages eines ihrer vielen Opfer sich gegen sie erheben und zurückschlagen wird! author: Albus Dumbledore)

In diesen schweren Tagen sind wir in Gedanken ganz bei unseren ukrainischen Mitmenschen!

----

Date: 2022-03-03

----

Subtitle-wanted: false

----

Subtitle: 