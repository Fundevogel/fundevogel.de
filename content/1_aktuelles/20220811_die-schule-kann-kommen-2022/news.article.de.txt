Title: Die Schule kann kommen!

----

Text:

Mit einem kurzen Blick in den Laden grüßen wir Euch aus unserem Laden - während draußen die Sonne unnachgiebig auf Freiburg brennt und sogar unsere "Bächle" mit der Wasserknappheit kämpfen, gibt es (fast) nichts Schöneres, als gemütlich zu stöbern und sich die Beute anschließend genüsslich einzuverleiben.

In Vorbereitung auf das Thema "Schule", das alljährlich eine neue Kindergeneration ereilt, haben wir unseren Thementisch entsprechend eingerichtet und bieten sowohl künftigen Jungleser:innen als auch deren schenkfreudigen Verwandten eine Auswahl an Erstlese- und Vorlesebüchern.

Für Lesegeübte ab zehn Jahren bieten wir zur Veröffentlichung des neuen "Willow"-Bandes neben den Büchern eine Reihe passender Nichtbücher - mehr dazu und unserem Thementisch "Endlich Schule!" findet Ihr auf den Bildern. Wir freuen uns auf Euch!

Euer
Fundevogel-Team

----

Date: 2022-08-11

----

Subtitle-wanted: true

----

Subtitle: Immer dran denken: nach den Ferien ist vor den Ferien