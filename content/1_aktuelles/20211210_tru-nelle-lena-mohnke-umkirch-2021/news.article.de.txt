Title: Grüße nach Umkirch: "Tru & Nelle" von Greg Neri

----

Text:

Die Lesetipps sind ein wichtiger Bestandteil unserer Webseite und werden monatlich erweitert. Kaum bekannt ist jedoch, dass sie auch im Gemeindeblatt von Umkirch erscheinen!

Darauf aufmerksam geworden, hat unsere Kundin Imke (link: https://freiburg.social/@imke/107387709056054003 text: einen freundlichen Gruß target: blank) auf der (link: wiki=Mastodon (Software) text: sozialen Plattform Mastodon) (einer interessanten Alternative zu Twitter) veröffentlicht, für den wir uns herzlich bedanken:

> Der (link: https://freiburg.social/@fundevogel text: @fundevogel target: blank) mit einer #buchvorstellung in unserem Gemeindeblättle.

Wir wünschen Imke und ihrer Familie und Euch allen eine starke Immunabwehr und einen besinnlichen 3. Advent!

Euer
*Fundevogel-Team*

----

Date: 2021-12-10

----

Subtitle-wanted: true

----

Subtitle: Fund der Woche
