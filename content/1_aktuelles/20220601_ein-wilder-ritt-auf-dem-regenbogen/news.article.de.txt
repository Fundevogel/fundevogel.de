Title: Ein wilder Ritt auf dem Regenbogen!

----

Text:

Am 1. Juni beginnt der alljährliche **Pride Month**, der weltweit gefeiert wird. In Deutschland kennt man eher den Christopher Street Day.

Seinen Ursprung findet der Pride Month im Juni in den späten 1960er Jahren in New York City. Homosexuelle und trans Menschen wurden wegen Unzucht in Gefängnisse oder Zuchthäuser gesperrt, von der Gesellschaft ausgegrenzt - auch in Deutschland.

Das Stonewall Inn, das sich in der Christopher Street in New York befand, war eine Bar, die sich explizit an Menschen aus der LGBTQ-Community richtete. Polizeirazzien und Polizeigewalt waren im Stonewall Inn an der Tagesordnung.

Eines Tages jedoch begann sich die Community zu wehren. Der 28. Juni am Stonewall Inn war der Auftakt für Proteste und Demonstrationen gegen die Diskriminierung und Polizeigewalt und Anstoß für die moderne LGBTQ-Bewegung, die sich für mehr Rechte und Gleichbehandlung einsetzt. Aus eben jener Zeit heraus entwickelten sich schließlich die bunten Pride-Paraden, die wir heute kennen, und der Pride Month, der jedes Jahr im Juni stattfindet.

*Aus diesem Anlass findet ihr im Juni bei uns einen Thementisch zum Thema LGBTQIA+ !*

----

Date: 2022-06-01

----

Subtitle-wanted: true

----

Subtitle: "Pride Month" im Fundevogel