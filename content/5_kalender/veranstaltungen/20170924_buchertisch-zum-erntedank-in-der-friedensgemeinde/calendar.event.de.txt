Title: Büchertisch zum Erntedank in der Friedensgemeinde

----

Location: Friedensgemeinde Hirzbergstraße

----

Date: 2017-09-24

----

Link:

----

Category: Büchertisch

----

Text:

Wie jedes Jahr freuen wir uns besonders am Fest der Friedensgemeinde anläßlich Erntedank mit einem Büchtisch teilnehmen zu dürfen. Immer wieder eine besondere Atmosphäre!

*offene Veranstaltung, ganztägig*

----

Timestart:

----

Timeend:

----

Dateend: 2017-09-24

----

Showtime: true

----

Multipledays: false

----

Draft: 1
