Title: Lecture en français - Lesung auf französisch

----

Location: Buchhandlung Fundevogel

----

Date: 2019-03-27

----

Link:

----

Category: Lesung

----

Text:

De conte en conte ... Promenons-nous dans les rues !
Début du parcours : 15h à la librairie
Fin : vers 17h au Café Marcel
Contenu : lecture, histoire filmée et goûter !
Pour enfants de 4 à 6 ans
Entrée libre - Sur inscription : 0761/25218 ou info@fundevogel.de

----

Timestart: 15:00:00

----

Timeend: 17:00:00

----

Dateend: 2019-03-27

----

Showtime: true

----

Multipledays: false

----

Draft: 1
