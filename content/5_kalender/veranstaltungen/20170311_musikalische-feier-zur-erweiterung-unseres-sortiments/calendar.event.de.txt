Title: Musikalische Feier zur Erweiterung unseres Sortiments

----

Location: Buchhandlung Fundevogel

----

Date: 2017-03-11

----

Link:

----

Category: Feier / Fest

----

Text: Am 11. März, 18 Uhr feiern wir bei uns im Fundevogel die Erweiterung unseres Sortiments um französische Bücher.  Die Präsentation unserer kleinen französischen Ecke wird mit Liedern von Boris Vian live begleitet. Wenn Ihr möchtet, dürft Ihr gerne auch die Klassiker der Chanson francaise mitsingen! Kommt und feiert mit!

----

Timestart: 18:00:00

----

Timeend:

----

Dateend: 2017-03-11

----

Showtime: true

----

Multipledays: false

----

Draft: 1
