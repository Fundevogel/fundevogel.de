Title: Kinderliteraturfest im Stadttheater

----

Location: Stadttheater Freiburg

----

Date: 2018-10-21

----

Link:

----

Category: Lirum Larum Lesefest

----

Text: Zum großen Abschlussfest des diesjährigen **Lirum Larum Lesefestes** im Stadttheater ist selbstverständlich auch der Fundevogel wieder mit dabei. Wir werden mit einem großen Büchertisch im Foyer des Stadttheaters an dem Kinderliteraturfest teilnehmen, an dem ihr alle Bücher der Gastautoren und noch so manches mehr finden werdet. Außerdem könnt ihr bei uns kreativ werden und bunte Lesezeichen basteln. Wir freuen uns schon darauf Euch alle persönlich zu treffen!

----

Timestart: 12:30:00

----

Timeend: 18:00:00

----

Dateend: 2018-10-21

----

Showtime: true

----

Multipledays: false

----

Draft: 1
