Title: Deutsch-französischer Tag für Familien

----

Location: Centre Culturel Français Freiburg  Münsterplatz 11 | im Kornhaus | 79098 Freiburg

----

Date: 2019-02-02

----

Link:

----

Category: Lesung

----

Text:

**Journée franco-allemande** - Wir feiern zusammen!
Zu Gast ist die Autorin** Aurélie Guetz** mit ihrem zweisprachigen Comic* Die Abenteuer von Kazh – Meine deutsch-französische Familie*.
Nach der **Lesung **findet ein **Zeichenworkshop **für 7 bis 12-Jährige statt. Für die Jüngeren gibt es ein **Bastelatelier **und **Sprachanimation **mit der FranceMobil-Lektorin. Traditionell zur chandeleur backen wir zum Abschluss leckere **Crêpes**.
Wir, aus der Buchhandlung Fundevogel, sind auch vor Ort und präsentieren Euch eine **Auswahl an Büchern auf deutsch und französisch**, für groß und klein.
Der Eintritt ist frei! Anmeldung bitte telefonisch: 0761 / 207 39 0 oder per Mail: u.beeck@ccf-fr.de

----

Timestart: 11:00:00

----

Timeend: 14:00:00

----

Dateend: 2019-02-02

----

Showtime: true

----

Multipledays: false

----

Draft: 1
