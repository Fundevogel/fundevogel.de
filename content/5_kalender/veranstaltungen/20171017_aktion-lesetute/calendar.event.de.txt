Title: Aktion Lesetüte

----

Location: Maria Sybilla Merian Grundschule Kiechlinsbergen Königschaffhausen

----

Date: 2017-10-17

----

Link:

----

Category: Schulinterne Veranstaltung

----

Text: Wir sind mit dem SC-Freiburg und dem Füchsle in Sachen "Fußball und lesen" unterwegs, um den Schülern einen guten Start ins neue Schuljahr zu wünschen. Die Erstklässler bekommen bei dieser Veranstaltung eine von uns gut gefüllte Lesetüte überreicht. Diese haben die älteren Schüler vor den Sommerferien für die Neuankömmlinge künstlerisch gestaltet. Als Dankeschön für ihr Engagement dürfen sich die Künstler ein Vorlesebuch für ihre Klasse aussuchen.

----

Timestart: 09:15:00

----

Timeend: 10:15:00

----

Dateend: 2017-10-17

----

Showtime: true

----

Multipledays: false

----

Draft: 1
