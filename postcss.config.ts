export default (ctx: any) => ({
    plugins: [
        require('postcss-import'),
        require('tailwindcss')({config: 'source/styles/tailwind.config.ts'}),
        require('autoprefixer'),
        ctx.env == 'minify' ? require('cssnano') : false
    ],
})
